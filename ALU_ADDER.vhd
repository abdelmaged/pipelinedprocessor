library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ALU_ADDER is
	port(
		AA, BB   : in  std_logic_vector(7 downto 0); -- ALU Operands
		carry_in : in  std_logic;
		ZNCV     : out std_logic_vector(3 downto 0); -- Flags: Zero-Negative-Carry-Overflow
		F_out    : out std_logic_vector(7 downto 0)
	);
end entity ALU_ADDER;

architecture A_ALU_ADDER of ALU_ADDER is
	signal temp_cout : std_logic;
	signal temp_Fout : std_logic_vector(7 downto 0);
	component my_nadder IS
		Generic(n : integer := 8);
		PORT(
			a, b : in  std_logic_vector(n - 1 downto 0);
			cin  : in  std_logic;
			s    : out std_logic_vector(n - 1 downto 0);
			cout : out std_logic);
	end component;

begin
	my_add : my_nadder generic map(8) port map(AA, BB, '0', temp_Fout, temp_cout);
	ZNCV(2) <= temp_cout;
	-- zero flag
	ZNCV(0) <= '1' when temp_Fout = "00000000"
		else '0';
	-- over flow flag
	ZNCV(3) <= '1' when (((AA(7) = '0') and (BB(7) = '0') and (temp_Fout(7) = '1')) or ((AA(7) = '1') and (BB(7) = '1') and (temp_Fout(7) = '0')))
		else '0';
	-- negative flag
	ZNCV(1) <= temp_Fout(7);            -- not sure from the logic :3
	F_out   <= temp_Fout;
end architecture A_ALU_ADDER;
