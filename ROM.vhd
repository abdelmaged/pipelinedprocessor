library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

Entity rom is
	port(
		read    : in  std_logic;
		address : in  std_logic_vector(7 downto 0);
		dataout : out std_logic_vector(7 downto 0));
end entity rom;

architecture a_rom of rom is
	type rom_type is array (0 to 255) of std_logic_vector(7 downto 0);
	signal rom : rom_type;

begin
	dataout <= rom(to_integer(unsigned(address))) when read = '1';
end architecture a_rom;

