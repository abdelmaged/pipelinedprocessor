vsim work.alu
# vsim work.alu 
# Loading std.standard
# Loading std.textio(body)
# Loading ieee.std_logic_1164(body)
# Loading ieee.numeric_std(body)
# Loading work.alu(rtl)
# Loading work.alu_adder(a_alu_adder)
# Loading work.my_nadder(a_my_nadder)
# Loading work.my_adder(a_my_adder)
# Loading work.alu_sub(a_alu_sub)
# Loading work.alu_dec(a_alu_dec)
# Loading work.alu_inc(a_alu_inc)
# Loading work.alu_neg(a_alu_neg)
# vsim work.alu 
# Loading std.standard
# Loading std.textio(body)
# Loading ieee.std_logic_1164(body)
# Loading ieee.numeric_std(body)
# Loading work.alu(rtl)
# Loading work.alu_adder(a_alu_adder)
# Loading work.my_nadder(a_my_nadder)
# Loading work.my_adder(a_my_adder)
# Loading work.alu_sub(a_alu_sub)
# Loading work.alu_dec(a_alu_dec)
# Loading work.alu_inc(a_alu_inc)
# Loading work.alu_neg(a_alu_neg)
add wave sim:/alu/*
force -freeze sim:/alu/A 00000001 0
force -freeze sim:/alu/B 00000011 0
force -freeze sim:/alu/MemoryAddreesIn 00000000 0
force -freeze sim:/alu/CCR_IN 0000 0
force -freeze sim:/alu/AluMode 100000 0
force -freeze sim:/alu/IN_PORT 00000000 0
run
# ** Warning: (vsim-WLF-5000) WLF file currently in use: vsim.wlf
# 
#           File in use by: unknown  Hostname: madel0093-Inspiron-N5110  ProcessID: 7542
# 
#           Attempting to use alternate WLF file "./wlftca1tw1".
# ** Warning: (vsim-WLF-5001) Could not open WLF file: vsim.wlf
# 
#           Using alternate file: ./wlftca1tw1
# 
run
run
force -freeze sim:/alu/AluMode 100001 0
run
force -freeze sim:/alu/AluMode 100010 0
run
force -freeze sim:/alu/AluMode 100011 0
run
force -freeze sim:/alu/B 11111111 0
run
force -freeze sim:/alu/AluMode 100010 0
run
force -freeze sim:/alu/AluMode 100001 0
run
force -freeze sim:/alu/AluMode 100000 0
run
force -freeze sim:/alu/AluMode 001100 0
run
run
force -freeze sim:/alu/A 00000111 0
force -freeze sim:/alu/B 00000100 0
run
force -freeze sim:/alu/B 00001101 0
run
force -freeze sim:/alu/opCode 1000 0
run
force -freeze sim:/alu/ra 01 0
run
force -freeze sim:/alu/ra 10 0
run
force -freeze sim:/alu/ra 11 0
run

force -freeze sim:/alu/ra 10 0
run

force -freeze sim:/alu/B 11111100 0

run