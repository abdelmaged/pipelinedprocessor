vsim -gui work.controlunit
add wave sim:/controlunit/*
force -freeze sim:/controlunit/Fetch_stage_output 00001100 0
run
force -freeze sim:/controlunit/Fetch_stage_output 00001000 0
run
force -freeze sim:/controlunit/Fetch_stage_output 00010100 0
force -freeze sim:/controlunit/Fetch_stage_output 000101011 0
run
force -freeze sim:/controlunit/Fetch_stage_output 00101111 0
run
force -freeze sim:/controlunit/Fetch_stage_output 00010010 0
run
force -freeze sim:/controlunit/Fetch_stage_output 00010110 0
run
