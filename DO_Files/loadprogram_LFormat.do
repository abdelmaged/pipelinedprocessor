vsim work.main
add wave sim:/main/clk
add wave sim:/main/rst
#add wave sim:/main/enable
add wave sim:/main/INTR
add wave sim:/main/IN_PORT
add wave sim:/main/OUT_PORT
add wave sim:/main/PC_reg/q

add wave sim:/main/FETCH_lbl/pc_out
add wave sim:/main/CU_lbl/Fetch_stage_output
add wave sim:/main/ALU_lbl/AluMode
add wave sim:/main/ALU_lbl/A
add wave sim:/main/ALU_lbl/B
add wave sim:/main/ALU_lbl/CCR_IN
add wave sim:/main/ALU_lbl/Y
add wave sim:/main/ALU_lbl/CCR_Out
add wave sim:/main/RAM_lbl/address_1
add wave sim:/main/RAM_lbl/datain_1
add wave sim:/main/RAM_lbl/dataout

add wave sim:/main/CCR_reg/q
add wave sim:/main/RF_lbl/R0/q
add wave sim:/main/RF_lbl/R1/q
add wave sim:/main/RF_lbl/R2/q
add wave sim:/main/RF_lbl/R3/q

add wave -position end  sim:/main/RAM_lbl/ram(0)
add wave -position end  sim:/main/RAM_lbl/ram(1)
add wave -position end  sim:/main/RAM_lbl/ram(2)
add wave -position end  sim:/main/RAM_lbl/ram(3)
add wave -position end  sim:/main/RAM_lbl/ram(4)
add wave -position end  sim:/main/RAM_lbl/ram(5)


mem load -i ./Assembler/testcases_v2/LFormat.mem /main/fetch_lbl/my_rom/rom
mem load -filltype inc -filldata 0 -fillradix symbolic -skip 0 /main/RAM_lbl/ram

mem load -filltype value -filldata 10 -fillradix decimal /main/RAM_lbl/ram(2)
mem load -filltype value -filldata 20 -fillradix decimal /main/RAM_lbl/ram(3)
mem load -filltype value -filldata 30 -fillradix decimal /main/RAM_lbl/ram(4)
mem load -filltype value -filldata 40 -fillradix decimal /main/RAM_lbl/ram(5)


force -freeze sim:/main/enable 1 0
force -freeze sim:/main/INTR 0 0
force -freeze sim:/main/CCR_enable 1 0
force -freeze sim:/main/clk 0 0, 1 {50 ps} -r 100
force -freeze sim:/main/rst 1 0
run
echo "Reset, time: $now\n"

force -freeze sim:/main/rst 0 0
run
echo ".2  LDM R0, 2 , IF, time: $now\n"

run
echo ".3  2         , IF, time: $now"
echo ".2  LDM R0, 2 , ID, time: $now\n"

run
echo ".4  LDM R1, 3 , IF, time: $now"
echo ".3  2         , ID, time: $now"
echo ".2  LDM R0, 2 , EX, time: $now\n"

run
echo ".5  3         , EX, time: $now"
echo ".4  LDM R1, 3 , ID, time: $now"
echo ".3  2         , EX, time: $now"
echo ".2  LDM R0, 2 , MA, time: $now\n"

run
echo ".6  LDD R2, 2 , IF, time: $now"
echo ".5  3         , ID, time: $now"
echo ".4  LDM R1, 3 , EX, time: $now"
echo ".3  2         , MA, time: $now"
echo ".2  LDM R0, 2 , WB, time: $now\n"

run
echo ".7  2         , IF, time: $now"
echo ".6  LDD R2, 2 , ID, time: $now"
echo ".5  3         , EX, time: $now"
echo ".4  LDM R1, 3 , MA, time: $now"
echo ".3  2         , WB, time: $now\n"

run
echo ".8  LDD R3, 3 , IF, time: $now"
echo ".7  2         , ID, time: $now"
echo ".6  LDD R2, 2 , EX, time: $now"
echo ".5  3         , MA, time: $now"
echo ".4  LDM R1, 3 , WB, time: $now\n"

run
echo ".9  3         , IF, time: $now"
echo ".8  LDD R3, 3 , ID, time: $now"
echo ".7  2         , EX, time: $now"
echo ".6  LDD R2, 2 , MA, time: $now"
echo ".5  3         , WB, time: $now\n"

run
exa -name -dec RAM_lbl/ram(3) RAM_lbl/ram(4) RAM_lbl/ram(5)
echo ".10 STD R2, 4 , IF, time: $now"
echo ".9  3         , ID, time: $now"
echo ".8  LDD R3, 3 , EX, time: $now"
echo ".7  2         , MA, time: $now"
echo ".6  LDD R2, 2 , WB, time: $now\n"

run
exa -name -dec RAM_lbl/ram(3) RAM_lbl/ram(4) RAM_lbl/ram(5)
echo ".11 4         , IF, time: $now"
echo ".10 STD R2, 4 , ID, time: $now"
echo ".9  3         , EX, time: $now"
echo ".8  LDD R3, 3 , MA, time: $now"
echo ".7  2         , WB, time: $now\n"

run
exa -name -dec RAM_lbl/ram(3) RAM_lbl/ram(4) RAM_lbl/ram(5)
echo ".12 STD R3, 5 , IF, time: $now"
echo ".11 4         , ID, time: $now"
echo ".10 STD R2, 4 , EX, time: $now"
echo ".9  3         , MA, time: $now"
echo ".8  LDD R3, 3 , WB, time: $now\n"

run
exa -name -dec RAM_lbl/ram(3) RAM_lbl/ram(4) RAM_lbl/ram(5)
echo ".13 5         , IF, time: $now"
echo ".12 STD R3, 5 , ID, time: $now"
echo ".11 4         , EX, time: $now"
echo ".10 STD R2, 4 , MA, time: $now"
echo ".9  3         , WB, time: $now\n"

run
exa -name -dec RAM_lbl/ram(3) RAM_lbl/ram(4) RAM_lbl/ram(5)
echo ".14 LDI R0, R3, IF, time: $now"
echo ".13 5         , ID, time: $now"
echo ".12 STD R3, 5 , EX, time: $now"
echo ".11 4         , MA, time: $now"
echo ".10 STD R2, 4 , WB, time: $now\n"


run
exa -name -dec RAM_lbl/ram(3) RAM_lbl/ram(4) RAM_lbl/ram(5)
echo ".15 Add R3, R0    , IF, time: $now"
echo ".14 LDI R0, R3 (1), ID, time: $now"
echo ".13 5             , EX, time: $now"
echo ".12 STD R3, 5     , MA, time: $now"
echo ".11 4             , WB, time: $now\n"


run
exa -name -dec RAM_lbl/ram(3) RAM_lbl/ram(4) RAM_lbl/ram(5)
echo ".15 Add R3, R0 STALL, IF, time: $now"
echo ".14 LDI R0, R3 (2)  , ID, time: $now"
echo ".14 LDI R0, R3 (1)  , EX, time: $now"
echo ".13 5               , MA, time: $now"
echo ".12 STD R3, 5       , WB, time: $now\n"

run
exa -name -dec RAM_lbl/ram(3) RAM_lbl/ram(4) RAM_lbl/ram(5)
echo ".16 STI R1, R3      , IF, time: $now"
echo ".15 Add R3, R0      , ID, time: $now"
echo ".14 LDI R0, R3 (2)  , EX, time: $now"
echo ".14 LDI R0, R3 (1)  , MA, time: $now"
echo ".13 5               , WB, time: $now\n"

run
exa -name -dec RAM_lbl/ram(3) RAM_lbl/ram(4) RAM_lbl/ram(5)
echo ".16 STI R1, R3      , ID, time: $now"
echo ".15 Add R3, R0      , EX, time: $now"
echo ".14 LDI R0, R3 (2)  , MA, time: $now"
echo ".14 LDI R0, R3 (1)  , WB, time: $now\n"

run
exa -name -dec RAM_lbl/ram(3) RAM_lbl/ram(4) RAM_lbl/ram(5)
echo ".16 STI R1, R3      , EX, time: $now"
echo ".15 Add R3, R0      , MA, time: $now"
echo ".14 LDI R0, R3 (2)  , WB, time: $now\n"

run
exa -name -dec RAM_lbl/ram(3) RAM_lbl/ram(4) RAM_lbl/ram(5)
echo ".16 STI R1, R3      , MA, time: $now"
echo ".15 Add R3, R0      , WB, time: $now\n"

run
exa -name -dec RAM_lbl/ram(3) RAM_lbl/ram(4) RAM_lbl/ram(5)
echo ".16 STI R1, R3      , WB, time: $now"