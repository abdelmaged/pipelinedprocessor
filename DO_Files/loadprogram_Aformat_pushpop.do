vsim work.main
add wave sim:/main/clk
add wave sim:/main/rst
#add wave sim:/main/enable
add wave sim:/main/INTR
add wave sim:/main/IN_PORT
add wave sim:/main/OUT_PORT
add wave sim:/main/PC_reg/q

add wave sim:/main/CU_lbl/Fetch_stage_output
add wave sim:/main/ALU_lbl/AluMode
add wave sim:/main/ALU_lbl/A
add wave sim:/main/ALU_lbl/B
add wave sim:/main/ALU_lbl/CCR_IN
add wave sim:/main/ALU_lbl/Y
add wave sim:/main/ALU_lbl/CCR_Out
add wave sim:/main/RAM_lbl/address_1
add wave sim:/main/RAM_lbl/datain_1
add wave sim:/main/RAM_lbl/dataout

add wave sim:/main/CCR_reg/q
add wave sim:/main/RF_lbl/R0/q
add wave sim:/main/RF_lbl/R1/q
add wave sim:/main/RF_lbl/R2/q
add wave sim:/main/RF_lbl/R3/q

add wave -position end  sim:/main/RAM_lbl/ram(250)
add wave -position end  sim:/main/RAM_lbl/ram(251)
add wave -position end  sim:/main/RAM_lbl/ram(252)
add wave -position end  sim:/main/RAM_lbl/ram(253)
add wave -position end  sim:/main/RAM_lbl/ram(254)
add wave -position end  sim:/main/RAM_lbl/ram(255)

mem load -i ./Assembler/testcases_v2/AFormat-WithPushPop.mem /main/FETCH_lbl/my_rom/rom
mem load -filltype inc -filldata 0 -fillradix symbolic -skip 0 /main/RAM_lbl/ram

force -freeze sim:/main/enable 1 0
force -freeze sim:/main/INTR 0 0
force -freeze sim:/main/clk 0 0, 1 {50 ps} -r 100
force -freeze sim:/main/rst 1 0
run
echo "Reset, time: $now"

force -freeze sim:/main/rst 0 0
force -freeze sim:/main/IN_PORT 45 0
run
echo "Filling the Pipeline, FETCH        , time: $now"
force -freeze sim:/main/IN_PORT 5 0
run
echo "Filling the Pipeline, DECODE       , time: $now"
noforce sim:/main/IN_PORT
run
echo "Filling the Pipeline, EXECUTE      , time: $now"
run
echo "Filling the Pipeline, MEMORY ACCESS, time: $now"
run
echo "Filling the Pipeline, WRITE BACK   , time: $now"
echo "In R0, time: $now"

run
echo "In R1, time: $now\n"

run
echo "Mov R2, R0, time: $now\n"

run
echo "ADD R2, R1, time: $now\n"

run
echo "time: $now\n"
run
echo "PUSH R0, time: $now\n"

run
echo "time: $now\n"
run
echo "PUSH R1, time: $now\n"

run
echo "time: $now\n"
run
echo "PUSH R2, time: $now\n"

run
echo "time: $now\n"
run
echo "time: $now\n"
run
echo "POP R0, time: $now\n"

run
echo "time: $now\n"

run
echo "time: $now\n"

run
echo "POP R1, time: $now\n"

run
echo "time: $now\n"

run
echo "time: $now\n"

run
echo "POP R2, time: $now\n"
echo "time: $now\n"

echo "AFormat_PushPop TEST FINISHED!, time: $now"
echo "NOTE: CCR is Updated after the Execution Stage"
#echo "Push take 2 clock cycles:\n\t1st clock cycle, STI\n\t2nd clock cyele, INC SP"
#echo "Pop  take 3 clock cycles:\n\t1st clock cyele, SP updated\n\t2nd clock cyele, Register updated"
#echo "\t3rd clock cyele, STALL"


