vsim work.main
#add wave -r /*
add wave sim:/main/*
add wave sim:/main/ALU_lbl/*
add wave -position insertpoint  \
sim:/main/RegesterFile_lbl/q0
add wave -position insertpoint  \
sim:/main/RegesterFile_lbl/q1 \
sim:/main/RegesterFile_lbl/q2 \
sim:/main/RegesterFile_lbl/q3

#To ini any register OPEN REGISTER FILE .VHDL AND YOU `ILL FIND 4 SIGNALS NAMED o0,1,2,3 ---> PUT YOUR INI VALUE INSIDE THEM :d 

force -freeze sim:/main/enable 1 0
force -freeze sim:/main/CCR_enable 1 0

mem load -filltype inc -filldata 0 -fillradix symbolic -skip 0 /main/RAM_lbl/ram
mem load -i rom1.mem /main/fetch_lbl/my_rom/rom

# add you IR here (it`ll be loaded at location 0 in the memory )
#   0<1 , 1<2 ---> 1<3   ---> 2<=3 ----> 1<<6
mem load -filltype value -filldata {00100100 } -fillradix symbolic /main/fetch_lbl/my_rom/rom(0)
mem load -filltype value -filldata {00101001 } -fillradix symbolic /main/fetch_lbl/my_rom/rom(1)
mem load -filltype value -filldata {00100110 } -fillradix symbolic /main/fetch_lbl/my_rom/rom(2)

force -freeze sim:/main/clk 1 0, 0 {50 ps} -r 100
force -freeze sim:/main/rst 1 0
run
force -freeze sim:/main/rst 0 0
run

