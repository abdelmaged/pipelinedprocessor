vsim work.main
add wave sim:/main/clk
add wave sim:/main/rst
#add wave sim:/main/enable
add wave sim:/main/INTR
add wave sim:/main/IN_PORT
add wave sim:/main/OUT_PORT
add wave sim:/main/PC_reg/q

add wave sim:/main/CU_lbl/Fetch_stage_output
add wave sim:/main/ALU_lbl/AluMode
add wave sim:/main/ALU_lbl/A
add wave sim:/main/ALU_lbl/B
add wave sim:/main/ALU_lbl/CCR_IN
add wave sim:/main/ALU_lbl/Y
add wave sim:/main/ALU_lbl/CCR_Out
add wave sim:/main/RAM_lbl/address_1
add wave sim:/main/RAM_lbl/datain_1
add wave sim:/main/RAM_lbl/dataout

add wave sim:/main/CCR_reg/q
add wave sim:/main/RF_lbl/R0/q
add wave sim:/main/RF_lbl/R1/q
add wave sim:/main/RF_lbl/R2/q
add wave sim:/main/RF_lbl/R3/q


mem load -i ./Assembler/testcases_v2/AFormat-WithoutPushPop.mem /main/FETCH_lbl/my_rom/rom
mem load -filltype inc -filldata 0 -fillradix symbolic -skip 0 /main/RAM_lbl/ram

force -freeze sim:/main/enable 1 0
force -freeze sim:/main/INTR 0 0
force -freeze sim:/main/clk 0 0, 1 {50 ps} -r 100
force -freeze sim:/main/rst 1 0
run
echo "Reset, time: $now"

force -freeze sim:/main/rst 0 0
force -freeze sim:/main/IN_PORT 45 0
run
echo "Filling the Pipeline, FETCH        , time: $now"
force -freeze sim:/main/IN_PORT 5 0
run
echo "Filling the Pipeline, DECODE       , time: $now"
force -freeze sim:/main/IN_PORT 70 0
run
echo "Filling the Pipeline, EXECUTE      , time: $now"
force -freeze sim:/main/IN_PORT 126 0
run
echo "Filling the Pipeline, MEMORY ACCESS, time: $now"
noforce sim:/main/IN_PORT
run
echo "Filling the Pipeline, WRITE BACK   , time: $now"
echo "IN R0 ;R0=45 , time: $now"
run
echo "IN R1 ;R1=5  , time: $now"
run
echo "IN R2 ;R2=70 , time: $now"
run
echo "IN R2 ;R2=126, time: $now"
run
echo "ADD R0, R1  ;R0 = 50d              , CCR=VCNZ=0000, time: $now"
run
echo "SUB R1, R3  ;R1 = -121d = 10000111b, CCR=VCNZ=0110, time: $now"
run
echo "AND R0, R1  ;R0 = 2d    = 00000010b, CCR=VCNZ=0100, time: $now"
run
echo "NOT R0      ;R0 = -3d   = 11111101b, CCR=VCNZ=0110, time: $now"
run
echo "ADD R3, R0  ;R3 = 123d  = 01111011b, CCR=VCNZ=0100, time: $now"
run
echo "RRC R0      ;R0 = -2d   = 11111110b, CCR=VCNZ=0100, time: $now"
run
echo "RRC R0      ;R0 = -1d   = 11111111b, CCR=VCNZ=0000, time: $now"
run
echo "RLC R2      ;R2 = 140d  = 10001100b, CCR=VCNZ=0000, time: $now"
run
echo "SUB R2, R2  ;R2 = 0d    =          , CCR=VCNZ=0001, time: $now"
run
echo "MOV R1, R3  ;R1 = 123d  =          , CCR=VCNZ=0001, time: $now"
run
echo "OUT R1      ;OUT PORT   = 123d     , CCR=VCNZ=0001, time: $now"
run
echo "NOP,                                                time: $now"
run
echo "NOP,                                                time: $now"
run
echo "NEG R0      ;R0 = 1d    = 00000001b, CCR=VCNZ=0000, time: $now"
run
echo "SETC        ;                      , CCR=VCNZ=0100, time: $now"
run
echo "RRC R2      ;R2 = -128  = 10000000b, CCR=VCNZ=0000, time: $now"
run
echo "NOT R2      ;R2 = 127d  = 01111111b, CCR=VCNZ=0000, time: $now"
run
echo "AND R2, R0  ;R2 = 1d    = 00000001b, CCR=VCNZ=0000, time: $now"
run
echo "RRC R2      ;R2 = 0d    = 00000000b, CCR=VCNZ=0100, time: $now"
run
echo "CLRC        ;                        CCR=VCNZ=0000, time: $now"
run
echo "INC R2      ;R2 = 1d    = 00000001b, CCR=VCNZ=0000, time: $now"
run
echo "RRC R2      ;R2 = 0d    = 00000000b, CCR=VCNZ=0100, time: $now"
run
echo "DEC R2      ;R2 = -1d   = 11111111b, CCR=VCNZ=0110, time: $now"

echo "AFormat_Without_PushPop TEST FINISHED!            , time: $now"
echo "NOTE: CCR is Updated after the Execution Stage"