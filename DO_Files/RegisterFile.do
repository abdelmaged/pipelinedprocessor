vsim work.RegesterFile(RTL) 

add wave -position end  sim:/regesterfile/Clk
add wave -position end  sim:/regesterfile/Rst
add wave -position end  sim:/regesterfile/writeSelection
add wave -position end  sim:/regesterfile/writeValue
add wave -position end  sim:/regesterfile/readSelection_0
add wave -position end  sim:/regesterfile/readValue_0
add wave -position end  sim:/regesterfile/readSelection_1
add wave -position end  sim:/regesterfile/readValue_1
add wave -position end  sim:/regesterfile/e0
add wave -position end  sim:/regesterfile/e1
add wave -position end  sim:/regesterfile/e2
add wave -position end  sim:/regesterfile/e3

add wave -position end  sim:/regesterfile/q0
add wave -position end  sim:/regesterfile/q1
add wave -position end  sim:/regesterfile/q2
add wave -position end  sim:/regesterfile/q3

echo "Reset, time: $now"
force -freeze sim:/regesterfile/Clk 0 0, 1 {50 ps} -r 100
force -freeze sim:/regesterfile/Rst 1 0

force -freeze sim:/regesterfile/writeSelection 00 0
force -freeze sim:/regesterfile/writeValue 8'h00 0

force -freeze sim:/regesterfile/readSelection_0 00 0
force -freeze sim:/regesterfile/readSelection_1 00 0
run

echo "Write to R0, time: $now"
force -freeze sim:/regesterfile/Rst 0 0
force -freeze sim:/regesterfile/writeSelection 00 0
force -freeze sim:/regesterfile/writeValue 8'h01 0
run

echo "Write to R1, time: $now"
force -freeze sim:/regesterfile/writeSelection 01 0
force -freeze sim:/regesterfile/writeValue 8'h02 0
run

echo "Write to R2, time: $now"
force -freeze sim:/regesterfile/writeSelection 10 0
force -freeze sim:/regesterfile/writeValue 8'h03 0
run

echo "Write to R3, time: $now"
force -freeze sim:/regesterfile/writeSelection 11 0
force -freeze sim:/regesterfile/writeValue 8'h04 0
run

echo "Read from R0, R1, time: $now"
force -freeze sim:/regesterfile/readSelection_0 00 0
force -freeze sim:/regesterfile/readSelection_1 01 0
run

echo "Read from R2, R3, time: $now"
force -freeze sim:/regesterfile/readSelection_0 10 0
force -freeze sim:/regesterfile/readSelection_1 11 0
run

echo "Write to R0 V(x0A) and Read from R0, time: $now"
force -freeze sim:/regesterfile/writeSelection 00 0
force -freeze sim:/regesterfile/writeValue 8'h0A 0

force -freeze sim:/regesterfile/readSelection_0 00 0
force -freeze sim:/regesterfile/readSelection_1 01 0
run