vsim work.alu
# vsim work.alu 
# Loading std.standard
# Loading std.textio(body)
# Loading ieee.std_logic_1164(body)
# Loading ieee.numeric_std(body)
# Loading work.alu(rtl)
# Loading work.alu_adder(a_alu_adder)
# Loading work.my_nadder(a_my_nadder)
# Loading work.my_adder(a_my_adder)
# Loading work.alu_sub(a_alu_sub)
# vsim work.alu 
# Loading std.standard
# Loading std.textio(body)
# Loading ieee.std_logic_1164(body)
# Loading ieee.numeric_std(body)
# Loading work.alu(rtl)
# Loading work.alu_adder(a_alu_adder)
# Loading work.my_nadder(a_my_nadder)
# Loading work.my_adder(a_my_adder)
add wave sim:/alu/*

force -freeze sim:/alu/A 00000101 0
force -freeze sim:/alu/B 00000110 0
force -freeze sim:/alu/MemoryAddreesIn 00000010 0
force -freeze sim:/alu/CCR_IN 0100 0
force -freeze sim:/alu/AluMode 010000 0
run

force -freeze sim:/alu/AluMode 010100 0
run
force -freeze sim:/alu/AluMode 001100 0
run
force -freeze sim:/alu/B 00000000 0
force -freeze sim:/alu/B 11111111 0
run
force -freeze sim:/alu/AluMode 010000 0
run
force -freeze sim:/alu/B 11111111 0
force -freeze sim:/alu/B 00000101 0
run
force -freeze sim:/alu/AluMode 001100 0
run
force -freeze sim:/alu/A 00000110 0
run
force -freeze sim:/alu/AluMode 010000 0
force -freeze sim:/alu/A 00000000 0
run
force -freeze sim:/alu/B 00000000 0
force -freeze sim:/alu/B 00000101 0
force -freeze sim:/alu/B 00000000 0
run
force -freeze sim:/alu/A 11111111 0
force -freeze sim:/alu/B 11111111 0
run
force -freeze sim:/alu/AluMode 001100 0
run
force -freeze sim:/alu/AluMode 001000 0
run
force -freeze sim:/alu/A 00000000 0
force -freeze sim:/alu/B 00000000 0
run
force -freeze sim:/alu/AluMode 001100 0
run