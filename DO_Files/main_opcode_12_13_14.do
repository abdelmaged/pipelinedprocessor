vsim work.main
#add wave -r /*
add wave -position end  sim:/main/AluMode
add wave -position end  sim:/main/AluOut
add wave -position end  sim:/main/Decode_stage_AluMode_OUT
add wave -position end  sim:/main/Decode_stage_MemOperation_OUT
add wave -position end  sim:/main/Decode_stage_WRback_OUT
add wave -position end  sim:/main/Execute_stage_AluOut_OUT
add wave -position end  sim:/main/Execute_stage_MemOperation_OUT
add wave -position end  sim:/main/Execute_stage_MemoryAdress
add wave -position end  sim:/main/Execute_stage_MemoryAdress_Alu_OUT
add wave -position end  sim:/main/Execute_stage_MemoryAdress_OUT
add wave -position end  sim:/main/Execute_stage_MemoryData_Alu_OUT
add wave -position end  sim:/main/Execute_stage_MemoryData_OUT
add wave -position end  sim:/main/Execute_stage_WRback_OUT
add wave -position end  sim:/main/MemOperation
add wave -position end  sim:/main/MemoryWrite
add wave -position end  sim:/main/Memory_stage_AluOut_OUT
add wave -position end  sim:/main/Memory_stage_MemOperation_OUT
add wave -position end  sim:/main/Memory_stage_MemoryData
add wave -position end  sim:/main/Memory_stage_MemoryData_OUT
add wave -position end  sim:/main/Memory_stage_WRback_OUT
add wave -position end  sim:/main/WRback
add wave -position end  sim:/main/Write_Back_Enable
add wave -position end  sim:/main/clk
add wave -position end  sim:/main/writeValue
add wave sim:/main/RAM_lbl/*
add wave sim:/main/ControlUnit_lbl/*
add wave sim:/main/ALU_lbl/*
add wave -position insertpoint  \
sim:/main/RegesterFile_lbl/q0
add wave -position insertpoint  \
sim:/main/RegesterFile_lbl/q1 \
sim:/main/RegesterFile_lbl/q2 \
sim:/main/RegesterFile_lbl/q3

#To ini any register OPEN REGISTER FILE .VHDL AND YOU `ILL FIND 4 SIGNALS NAMED o0,1,2,3 ---> PUT YOUR INI VALUE INSIDE THEM :d 

force -freeze sim:/main/enable 1 0
force -freeze sim:/main/CCR_enable 1 0

mem load -filltype inc -filldata 0 -fillradix symbolic -skip 0 /main/RAM_lbl/ram
mem load -i rom1.mem /main/fetch_lbl/my_rom/rom


# LDM, r0 // imm = xF0 // r0 = xF0
mem load -filltype value -filldata {11000000 } -fillradix symbolic /main/fetch_lbl/my_rom/rom(0)
mem load -filltype value -filldata {11110000 } -fillradix symbolic /main/fetch_lbl/my_rom/rom(1)
# LDD, r0 // ea = x0A with Data = x0F // r0 <= M[10] = x0F
mem load -filltype value -filldata {11000100 } -fillradix symbolic /main/fetch_lbl/my_rom/rom(2)
mem load -filltype value -filldata {00001010 } -fillradix symbolic /main/fetch_lbl/my_rom/rom(3)
mem load -filltype value -filldata {00001111 } -fillradix symbolic /main/RAM_lbl/ram(10)
# STD, // ea = x0A // M[ea] = M[x0A] <= r1 = 2 
mem load -filltype value -filldata {11001001 } -fillradix symbolic /main/fetch_lbl/my_rom/rom(4)
mem load -filltype value -filldata {00001010 } -fillradix symbolic /main/fetch_lbl/my_rom/rom(5)
# LDI R[rb] ← M[R[ra]]; //r0 = M[r1] = M[2] = 01010101
mem load -filltype value -filldata {11010100 } -fillradix symbolic /main/fetch_lbl/my_rom/rom(6)
mem load -filltype value -filldata {01010101 } -fillradix symbolic /main/RAM_lbl/ram(2)
#STI M[R[ra]] ←R[rb]; // M[r1] = M[2] <= r0 = 00001111
mem load -filltype value -filldata {11100100 } -fillradix symbolic /main/fetch_lbl/my_rom/rom(7)


#force -freeze sim:/main/clk 1 0, 0 {50 ps} -r 100
force -freeze sim:/main/clk 0 0, 1 {50 ps} -r 100
force -freeze sim:/main/rst 1 0
run
force -freeze sim:/main/rst 0 0
run

