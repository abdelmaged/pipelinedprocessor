vsim work.ALU(RTL)  

add wave -position end  sim:/alu/A
add wave -position end  sim:/alu/B
add wave -position end  sim:/alu/Y

add wave -position end  sim:/alu/AluMode
add wave -position end  sim:/alu/opCode
add wave -position end  sim:/alu/ra

add wave -position end  sim:/alu/CCR_IN
add wave -position end  sim:/alu/Z
add wave -position end  sim:/alu/N
add wave -position end  sim:/alu/C
add wave -position end  sim:/alu/V
add wave -position end  sim:/alu/CCR_Out

add wave -position end  sim:/alu/MemoryAddreesIn
add wave -position end  sim:/alu/MemoryAddreesOut
add wave -position end  sim:/alu/MemoryDataout

echo "Init A,B,Flags with dummy values x0F,x0F,x0"
force -freeze sim:/alu/A 8'h0F 0
force -freeze sim:/alu/B 8'h0F 0
force -freeze sim:/alu/CCR_IN 0000 0
force -freeze sim:/alu/MemoryAddreesIn 8'h00 0

echo "Try AluMode: opCode 6, ra 0, RLC, time: $now"
force -freeze sim:/alu/AluMode 011000 0
run

echo "Try AluMode: opCode 6, ra 1, RRC, time: $now"
force -freeze sim:/alu/AluMode 011001 0
run 

echo "Try AluMode: opCode 6, ra 2, SETC, time: $now"
force -freeze sim:/alu/AluMode 011010 0
run

echo "Try AluMode: opCode 6, ra 3, CLRC, time: $now"
force -freeze sim:/alu/AluMode 011011 0
run


