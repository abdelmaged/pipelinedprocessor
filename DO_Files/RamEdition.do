vsim work.ram
# vsim work.ram 
# Loading std.standard
# Loading std.textio(body)
# Loading ieee.std_logic_1164(body)
# Loading ieee.numeric_std(body)
# Loading work.ram(syncrama)
# WARNING: No extended dataflow license exists
add wave sim:/ram/*
# ** Warning: (vsim-WLF-5000) WLF file currently in use: vsim.wlf
# 
#           File in use by: unknown  Hostname: madel0093-Inspiron-N5110  ProcessID: 7542
# 
#           Attempting to use alternate WLF file "./wlftws2s6i".
# ** Warning: (vsim-WLF-5001) Could not open WLF file: vsim.wlf
# 
#           Using alternate file: ./wlftws2s6i
# 
force -freeze sim:/ram/clk 1 0, 0 {50 ps} -r 100
force -freeze sim:/ram/we 1 0
force -freeze sim:/ram/address_1 00000110 0
force -freeze sim:/ram/address_2 00000000 0
force -freeze sim:/ram/address_3 00000001 0
force -freeze sim:/ram/datain_1 11110000 0
force -freeze sim:/ram/datain_2 00001111 0
force -freeze sim:/ram/datain_3 00000110 0
run
force -freeze sim:/ram/address_1 00000010 0
force -freeze sim:/ram/address_2 00000011 0
force -freeze sim:/ram/address_3 00000100 0
force -freeze sim:/ram/datain_1 00000010 0
force -freeze sim:/ram/datain_2 00000011 0
force -freeze sim:/ram/datain_3 00000100 0
run
force -freeze sim:/ram/address_2 00000010 0
force -freeze sim:/ram/address_2 00000111 0
force -freeze sim:/ram/address_1 00000111 0
force -freeze sim:/ram/datain_1 11111110 0
force -freeze sim:/ram/datain_2 11111111 0
run
force -freeze sim:/ram/address_3 00001000 0
force -freeze sim:/ram/address_1 00000100 0
force -freeze sim:/ram/datain_3 00000101 0
run
force -freeze sim:/ram/address_1 00001001 0
force -freeze sim:/ram/address_3 00001001 0
run -continue
run
force -freeze sim:/ram/address_1 00001010 0
force -freeze sim:/ram/address_2 00001010 0
force -freeze sim:/ram/address_3 00001010 0
run
