vsim work.main
add wave sim:/main/clk
add wave sim:/main/rst
#add wave sim:/main/enable
add wave sim:/main/INTR
add wave sim:/main/IN_PORT
add wave sim:/main/OUT_PORT
add wave sim:/main/PC_reg/q

add wave sim:/main/FETCH_lbl/pc_out
add wave sim:/main/CU_lbl/Fetch_stage_output
add wave sim:/main/ALU_lbl/AluMode
add wave sim:/main/ALU_lbl/A
add wave sim:/main/ALU_lbl/B
add wave sim:/main/ALU_lbl/CCR_IN
add wave sim:/main/ALU_lbl/Y
add wave sim:/main/ALU_lbl/CCR_Out
add wave sim:/main/RAM_lbl/address_1
add wave sim:/main/RAM_lbl/datain_1
add wave sim:/main/RAM_lbl/dataout

add wave sim:/main/CCR_reg/q
add wave sim:/main/RF_lbl/R0/q
add wave sim:/main/RF_lbl/R1/q
add wave sim:/main/RF_lbl/R2/q
add wave sim:/main/RF_lbl/R3/q

add wave sim:/main/RAM_lbl/ram(250)
add wave sim:/main/RAM_lbl/ram(251)
add wave sim:/main/RAM_lbl/ram(252)
add wave -position end  sim:/main/RAM_lbl/ram(253)
add wave -position end  sim:/main/RAM_lbl/ram(254)
add wave -position end  sim:/main/RAM_lbl/ram(255)

mem load -i ./Assembler/testcases_v2/BFormat-Interrupt.mem /main/FETCH_lbl/my_rom/rom
mem load -filltype inc -filldata 0 -fillradix symbolic -skip 0 /main/RAM_lbl/ram

force -freeze sim:/main/enable 1 0
force -freeze sim:/main/INTR 0 0
force -freeze sim:/main/CCR_enable 1 0
force -freeze sim:/main/clk 0 0, 1 {50 ps} -r 100
force -freeze sim:/main/rst 1 0
run
echo "Reset, time: $now\n"

force -freeze sim:/main/rst 0 0
force -freeze sim:/main/IN_PORT 15 0
run
echo ".2 In 15, IF, time: $now\n"

force -freeze sim:/main/IN_PORT 30 0
run
echo ".3 In 30, IF, time: $now"
echo ".2 In 15, ID, time: $now\n"

noforce sim:/main/IN_PORT
run
echo ".4 NOP  , IF, time: $now"
echo ".3 In 30, ID, time: $now"
echo ".2 In 15, EX, time: $now\n"

#force -freeze sim:/main/INTR 1 0
#echo "!! Int at Fetch of Sub"
run
echo ".5 NOP  , IF, time: $now"
echo ".4 NOP  , ID, time: $now"
echo ".3 In 30, EX, time: $now"
echo ".2 In 15, MA, time: $now\n"

force -freeze sim:/main/INTR 1 0
echo "!! Int at Decode of Sub"
#force -freeze sim:/main/INTR 0 0
run
echo ".6 SUB R0, R0, IF, time: $now"
echo ".5 NOP       , ID, time: $now"
echo ".4 NOP       , EX, time: $now"
echo ".3 In 30     , MA, time: $now"
echo ".2 In 15     , WR, time: $now\n"

#force -freeze sim:/main/INTR 1 0
#echo "!! Int at Execute of Sub"
force -freeze sim:/main/INTR 0 0
run
echo ".7 NOP       , IF, time: $now"
echo ".6 SUB R0, R0, ID, time: $now"
echo ".5 NOP       , EX, time: $now"
echo ".4 NOP       , MA, time: $now"
echo ".3 In 30     , WB, time: $now\n"

force -freeze sim:/main/INTR 0 0
run
exa -name -dec RAM_lbl/ram(255)
exa -name -dec RAM_lbl/ram(254)
exa -name -dec RAM_lbl/ram(253)
echo ".? ISR       , IF, time: $now"
echo ".7 NOP       , ID, time: $now"
echo ".6 SUB R0, R0, EX, time: $now"
echo ".5 NOP       , MA, time: $now"
echo ".4 NOP       , WB, time: $now\n"

run
echo ".125 PUSH R0 , ID, time: $now"
echo ".? ISR       , IF, time: $now"
echo ".7 NOP       , ID, time: $now"
echo ".6 SUB R0, R0, MA, time: $now"
echo ".5 NOP       , WB, time: $now\n"

run
echo ".126 ADD R0, R1      , IF, time: $now"
echo ".125 PUSH R0      (1), ID, time: $now"
echo ".? ISR               , ID, time: $now"
echo ".7 NOP               , MA, time: $now"
echo ".6 SUB R0, R0        , WB, time: $now\n"

run
echo ".126 ADD R0, R1 STALL, IF, time: $now"
echo ".125 PUSH R0      (2), ID, time: $now"
echo ".125 PUSH R0      (1), EX, time: $now"
echo ".? ISR               , MA, time: $now"
echo ".7 NOP               , WB, time: $now\n"

run
echo ".127 POP R0          , IF, time: $now"
echo ".126 ADD R0, R1      , ID, time: $now"
echo ".125 PUSH R0      (2), EX, time: $now"
echo ".125 PUSH R0      (1), MA, time: $now"
echo ".? ISR               , WB, time: $now\n"

run
echo ".128 RTI             , IF, time: $now"
echo ".127 POP R0       (1), ID, time: $now"
echo ".126 ADD R0, R1      , EX, time: $now"
echo ".125 PUSH R0      (2), MA, time: $now"
echo ".125 PUSH R0      (1), WB, time: $now\n"

run
echo ".128 RTI STALL       , IF, time: $now"
echo ".127 POP R0       (2), ID, time: $now"
echo ".127 POP R0       (1), EX, time: $now"
echo ".126 ADD R0, R1      , MA, time: $now"
echo ".125 PUSH R0      (2), WB, time: $now\n"

run
echo ".128 RTI STALL       , IF, time: $now"
echo ".127 POP R0       (3), ID, time: $now"
echo ".127 POP R0       (2), EX, time: $now"
echo ".127 POP R0       (1), MA, time: $now"
echo ".126 ADD R0, R1      , WB, time: $now\n"

run
echo ".129 ???             , IF, time: $now"
echo ".128 RTI          (1), ID, time: $now"
echo ".127 POP R0       (3), EX, time: $now"
echo ".127 POP R0       (2), MA, time: $now"
echo ".127 POP R0       (1), WB, time: $now\n"

run
echo ".129 ???  STALL      , IF, time: $now"
echo ".128 RTI          (2), ID, time: $now"
echo ".128 RTI          (1), EX, time: $now"
echo ".127 POP R0       (3), MA, time: $now"
echo ".127 POP R0       (2), WB, time: $now\n"

run
echo ".129 ???  STALL      , IF, time: $now"
echo ".128 RTI          (3), ID, time: $now"
echo ".128 RTI          (2), EX, time: $now"
echo ".128 RTI          (1), MA, time: $now"
echo ".127 POP R0       (3), WB, time: $now\n"

run
echo ".129 ???  STALL      , IF, time: $now"
echo ".128 RTI          (4), ID, time: $now"
echo ".128 RTI          (3), EX, time: $now"
echo ".128 RTI          (2), MA, time: $now"
echo ".128 RTI          (1), WB, time: $now\n"

run
echo ".129 ???  STALL      , IF, time: $now"
echo ".128 RTI          (5), ID, time: $now"
echo ".128 RTI          (4), EX, time: $now"
echo ".128 RTI          (3), MA, time: $now"
echo ".128 RTI          (2), WB, time: $now\n"

run
echo ".129 ???  STALL      , IF, time: $now"
echo ".128 RTI          (6), ID, time: $now"
echo ".128 RTI          (5), EX, time: $now"
echo ".128 RTI          (4), MA, time: $now"
echo ".128 RTI          (3), WB, time: $now\n"

run
echo ".8 NOP               , IF, time: $now"
echo "STALL                , ID, time: $now"
echo ".128 RTI          (6), EX, time: $now"
echo ".128 RTI          (5), MA, time: $now"
echo ".128 RTI          (4), WB, time: $now"