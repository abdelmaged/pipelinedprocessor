vsim work.main
add wave sim:/main/clk
add wave sim:/main/rst
#add wave sim:/main/enable
add wave sim:/main/INTR
add wave sim:/main/IN_PORT
add wave sim:/main/OUT_PORT
add wave sim:/main/PC_reg/q

add wave sim:/main/FETCH_lbl/pc_out
add wave sim:/main/CU_lbl/Fetch_stage_output
add wave sim:/main/ALU_lbl/AluMode
add wave sim:/main/ALU_lbl/A
add wave sim:/main/ALU_lbl/B
add wave sim:/main/ALU_lbl/CCR_IN
add wave sim:/main/ALU_lbl/Y
add wave sim:/main/ALU_lbl/CCR_Out
add wave sim:/main/RAM_lbl/address_1
add wave sim:/main/RAM_lbl/datain_1
add wave sim:/main/RAM_lbl/dataout

add wave sim:/main/CCR_reg/q
add wave sim:/main/RF_lbl/R0/q
add wave sim:/main/RF_lbl/R1/q
add wave sim:/main/RF_lbl/R2/q
add wave sim:/main/RF_lbl/R3/q

add wave -position end  sim:/main/RAM_lbl/ram(4)
add wave -position end  sim:/main/RAM_lbl/ram(5)
add wave -position end  sim:/main/RAM_lbl/ram(254)
add wave -position end  sim:/main/RAM_lbl/ram(255)


mem load -i ./Assembler/testcases_v2/BFormat-Branch.mem /main/FETCH_lbl/my_rom/rom
mem load -filltype inc -filldata 0 -fillradix symbolic -skip 0 /main/RAM_lbl/ram

force -freeze sim:/main/enable 1 0
force -freeze sim:/main/INTR 0 0
force -freeze sim:/main/clk 0 0, 1 {50 ps} -r 100
force -freeze sim:/main/rst 1 0
run
echo "Reset, time: $now"


force -freeze sim:/main/rst 0 0
force -freeze sim:/main/IN_PORT 15 0
run
echo ".2 In R0, IF time: $now\n"

force -freeze sim:/main/IN_PORT 30 0
run
echo ".2 In R1, IF time: $now"
echo ".2 In R0, ID time: $now\n"

force -freeze sim:/main/IN_PORT 75 0
run
echo ".2 In R2, IF, time: $now"
echo ".2 In R1, ID, time: $now"
echo ".2 In R0, EX, time: $now\n"

force -freeze sim:/main/IN_PORT 5 0
run
echo ".2 In R3, IF, time: $now"
echo ".2 In R2, ID, time: $now"
echo ".2 In R1, EX, time: $now"
echo ".2 In R0, MA, time: $now\n"

noforce sim:/main/IN_PORT
run
echo ".2 JMP R0, IF, time: $now"
echo ".2 In R3 , ID, time: $now"
echo ".2 In R2 , EX, time: $now"
echo ".2 In R1 , MA, time: $now"
echo ".2 In R0 , WB, time: $now\n"

run
echo ".15 SUB R0, R0, IF, time: $now"
echo ".2 JMP R0     , ID, time: $now"
echo ".2 In R3      , EX, time: $now"
echo ".2 In R2      , MA, time: $now"
echo ".2 In R1      , WB, time: $now\n"

run
echo ".15 JZ R1     , IF, time: $now"
echo ".15 SUB R0, R0, ID, time: $now"
echo ".2 JMP R0     , EX, time: $now"
echo ".2 In R3      , MA, time: $now"
echo ".2 In R2      , WB, time: $now\n"

run
echo ".30 SUB R2, R1, IF, time: $now"
echo ".15 JZ R1     , ID, time: $now"
echo ".15 SUB R0, R0, EX, time: $now"
echo ".2 JMP R0     , MA, time: $now"
echo ".2 In R3      , WB, time: $now\n"

run
echo ".30 JN R0     , IF, time: $now"
echo ".30 SUB R2, R1, ID, time: $now"
echo ".15 JZ R1     , EX, time: $now"
echo ".15 SUB R0, R0, MA, time: $now"
echo ".2 JMP R0     , WB, time: $now\n"

run
exa -name -dec RAM_lbl/ram(5)
echo ".30 CALL R2   , IF, time: $now"
echo ".30 JN R0     , ID, time: $now"
echo ".30 SUB R2, R1, EX, time: $now"
echo ".15 JZ R1     , MA, time: $now"
echo ".15 SUB R0, R0, WB, time: $now\n"    

run
exa -name -dec RAM_lbl/ram(5)
echo "STALL         , IF, time: $now"
echo ".30 CALL R2   , ID, time: $now"
echo ".30 JN R0     , EX, time: $now"
echo ".30 SUB R2, R1, MA, time: $now"
echo ".15 JZ R1     , WB, time: $now\n"

run
exa -name -dec RAM_lbl/ram(5)
echo "STALL         , IF, time: $now"
echo "STALL         , IF, time: $now"
echo ".30 CALL R2   , EX, time: $now"
echo ".30 JN R0     , MA, time: $now"
echo ".30 SUB R2, R1, WB, time: $now\n"

run
exa -name -dec RAM_lbl/ram(5)
echo ".45 ADD R0, R1, IF, time: $now"
echo "STALL         , ID, time: $now"
echo " EX STALL"
echo ".30 CALL R2   , MA, time: $now"
echo ".30 JN R0     , WB, time: $now\n"

run
exa -name -dec RAM_lbl/ram(5)
echo ".45 RET       , IF, time: $now"
echo ".45 ADD R0, R1, ID, time: $now"
echo " EX STALL"
echo " MA STALL"
echo ".30 CALL R2   , WB, time: $now\n"

run
echo "STALL         , IF, time: $now"
echo ".45 RET       , ID, time: $now"
echo ".45 ADD R0, R1, EX, time: $now"
echo "STALL         , MA, time: $now"
echo "STALL         , WB, time: $now\n"

run
echo "STALL         , IF, time: $now"
echo "STALL         , ID, time: $now"
echo ".45 RET       , EX, time: $now"
echo ".45 ADD R0, R1, MA, time: $now"
echo "STALL         , WB, time: $now"

run
echo "STALL         , IF, time: $now"
echo "STALL         , IF, time: $now"
echo "STALL         , EX, time: $now"
echo ".45 RET       , MA, time: $now"
echo ".45 ADD R0, R1, WB, time: $now\n"

run
echo "STALL         , IF, time: $now"
echo "STALL         , IF, time: $now"
echo "STALL         , EX, time: $now"
echo ".45 RET       , MA, time: $now"
echo ".45 ADD R0, R1, WB, time: $now\n"

run
echo "STALL         , IF, time: $now"
echo "STALL         , ID, time: $now"
echo "STALL         , EX, time: $now"
echo "STALL         , MA, time: $now"
echo ".45 RET       , WB, time: $now\n"

run
echo ".30 ADD R0, R1, IF, time: $now"
echo "STALL         , ID, time: $now"
echo "STALL         , EX, time: $now"
echo "STALL         , MA, time: $now"
echo ".45 RET       , WB, time: $now\n"

run
echo ".30 JMP R0    , IF, time: $now"
echo ".30 ADD R0, R1, ID, time: $now"
echo "STALL         , EX, time: $now"
echo "STALL         , MA, time: $now"
echo "STALL         , WB, time: $now\n"

run
echo ".60 ADD R2,R3 , IF, time: $now"
echo ".30 JMP R0    , ID, time: $now"
echo ".30 ADD R0, R1, EX, time: $now"
echo "STALL         , MA, time: $now"
echo "STALL         , WB, time: $now\n"

run
echo ".60 LOOP R3, R0, IF, time: $now"
echo ".60 ADD R2,R3  , ID, time: $now"
echo ".30 JMP R0     , EX, time: $now"
echo ".30 ADD R0, R1 , MA, time: $now"
echo "STALL          , WB, time: $now\n"

run
echo "STALL          , IF, time: $now"
echo ".60 LOOP R3, R0, ID, time: $now"
echo ".60 ADD R2,R3  , EX, time: $now"
echo ".30 JMP R0     , MA, time: $now"
echo ".30 ADD R0, R1 , WB, time: $now\n"

run
echo ".60 ADD R2,R3  , IF, time: $now"
echo "STALL          , ID, time: $now"
echo ".60 LOOP R3, R0, EX, time: $now"
echo ".60 ADD R2,R3  , MA, time: $now"
echo ".30 JMP R0     , WB, time: $now\n"

run
echo ".60 LOOP R3, R0, IF, time: $now"
echo ".60 ADD R2,R3  , ID, time: $now"
echo "STALL          , EX, time: $now"
echo ".60 LOOP R3, R0, MA, time: $now"
echo ".60 ADD R2,R3  , WB, time: $now\n"

run
echo "STALL          , IF, time: $now"
echo ".60 LOOP R3, R0, ID, time: $now"
echo ".60 ADD R2,R3  , EX, time: $now"
echo "STALL          , MA, time: $now"
echo ".60 LOOP R3, R0, WB, time: $now\n"

run
echo ".60 ADD R2,R3  , IF, time: $now"
echo "STALL          , ID, time: $now"
echo ".60 LOOP R3, R0, EX, time: $now"
echo ".60 ADD R2,R3  , MA, time: $now"
echo "STALL          , WB, time: $now\n"

run
echo ".60 LOOP R3, R0, IF, time: $now"
echo ".60 ADD R2,R3  , ID, time: $now"
echo "STALL          , EX, time: $now"
echo ".60 LOOP R3, R0, MA, time: $now"
echo ".60 ADD R2,R3  , WB, time: $now\n"

run
echo "STALL          , IF, time: $now"
echo ".60 LOOP R3, R0, ID, time: $now"
echo ".60 ADD R2,R3  , EX, time: $now"
echo "STALL          , MA, time: $now"
echo ".60 LOOP R3, R0, WB, time: $now\n"

run
echo ".60 ADD R2,R3  , IF, time: $now"
echo "STALL          , ID, time: $now"
echo ".60 LOOP R3, R0, EX, time: $now"
echo ".60 ADD R2,R3  , MA, time: $now"
echo "STALL          , WB, time: $now\n"

run
echo ".60 LOOP R3, R0, IF, time: $now"
echo ".60 ADD R2,R3  , ID, time: $now"
echo "STALL          , EX, time: $now"
echo ".60 LOOP R3, R0, MA, time: $now"
echo ".60 ADD R2,R3  , WB, time: $now\n"

run
echo "STALL          , IF, time: $now"
echo ".60 LOOP R3, R0, ID, time: $now"
echo ".60 ADD R2,R3  , EX, time: $now"
echo "STALL          , MA, time: $now"
echo ".60 LOOP R3, R0, WB, time: $now\n"

run
echo ".60 ADD R2,R3  , IF, time: $now"
echo "STALL          , ID, time: $now"
echo ".60 LOOP R3, R0, EX, time: $now"
echo ".60 ADD R2,R3  , MA, time: $now"
echo "STALL          , WB, time: $now\n"

run
echo ".62            , IF, time: $now"
echo ".60 ADD R2,R3  , ID, time: $now"
echo "STALL          , EX, time: $now"
echo ".60 LOOP R3, R0, MA, time: $now"
echo ".60 ADD R2,R3  , WB, time: $now\n"

run
echo "STALL          , IF, time: $now"
echo ".62            , ED, time: $now"
echo ".60 ADD R2,R3  , EX, time: $now"
echo "STALL          , MA, time: $now"
echo ".60 LOOP R3, R0, WB, time: $now\n"

run
echo ".63            , IF, time: $now"
echo "STALL          , ID, time: $now"
echo ".62            , EX, time: $now"
echo ".60 ADD R2,R3  , MA, time: $now"
echo "STALL          , WB, time: $now\n"

run
echo ".64            , IF, time: $now"
echo ".63            , ID, time: $now"
echo "STALL          , EX, time: $now"
echo ".62            , MA, time: $now"
echo ".60 ADD R2,R3  , WB, time: $now\n"

#echo "NOTE: CALL take 2 clock cycle\n\t1 clock cycle, PUSH R3\n\t2 clock cycle, Jump R3"
#echo "NOTE: RET take 4 clock cycle\n\t1,2,3 clock cycle, POP R3\n\t4 clock cycle, Jump R3"
#echo "NOTE: LOOP take 2 clock cycle\n\t1 clock cycle, DEC Ra\n\t2 clock cycle, Jump Rb"

# NOT TAKEN INSTURCTIONS
#run
#echo ".2 SUB R2, R1, time: $now"
#exa -name -dec RF_lbl/R2/q
#exa -name -dec CCR_reg/q

#run
#echo ".15 SUB R2, R1 , time: $now"
#exa -name -dec RF_lbl/R2/q
#exa -name -dec CCR_reg/q