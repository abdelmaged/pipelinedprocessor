vsim work.main
#add wave -r /*
add wave sim:/main/clk
add wave sim:/main/rst
add wave sim:/main/enable
add wave sim:/main/INTR
add wave sim:/main/PCin
add wave sim:/main/PCOut
add wave sim:/main/PC_IS_RB
add wave sim:/main/FetchPcOut
add wave sim:/main/Fetch_stage_input
add wave sim:/main/Fetch_stage_output
add wave sim:/main/ALU_lbl/Z
add wave sim:/main/ALU_lbl/N
add wave sim:/main/ALU_lbl/C
add wave sim:/main/ALU_lbl/V
add wave sim:/main/CCR_IN
add wave sim:/main/CCR_OUT
add wave -position insertpoint  \
sim:/main/RegesterFile_lbl/q0 \
sim:/main/RegesterFile_lbl/q1 \
sim:/main/RegesterFile_lbl/q2 \
sim:/main/RegesterFile_lbl/q3


mem load -i ./DO_Files/jumps.mem /main/fetch_lbl/my_rom/rom
mem load -filltype inc -filldata 0 -fillradix symbolic -skip 0 /main/RAM_lbl/ram

force -freeze sim:/main/enable 1 0
force -freeze sim:/main/INTR 0 0
force -freeze sim:/main/CCR_enable 1 0
force -freeze sim:/main/clk 0 0, 1 {50 ps} -r 100
force -freeze sim:/main/rst 1 0
run

force -freeze sim:/main/rst 0 0
run

