vsim -gui work.alu
add wave sim:/alu/*
force -freeze sim:/alu/MemoryAddreesIn 000000100 0
force -freeze sim:/alu/CCR_IN 0000 0
force -freeze sim:/alu/AluMode 000011 0
run
force -freeze sim:/alu/AluMode 000111 0
force -freeze sim:/alu/A 00001000 0
force -freeze sim:/alu/B 00001100 0
run
force -freeze sim:/alu/CCR_IN 0100 0
run
force -freeze sim:/alu/B 00001101 0
run
force -freeze sim:/alu/AluMode 0001011 0
run
force -freeze sim:/alu/B 00000000 0
run
force -freeze sim:/alu/A 10000000 0
force -freeze sim:/alu/B 10000000 0
run
force -freeze sim:/alu/A 00000000 0
force -freeze sim:/alu/B 00000000 0
force -freeze sim:/alu/CCR_IN 0000 0
run