library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ForwardUnit is
	port(
		RegisterNumber                                                                                                                    : in  std_logic_vector(1 downto 0);
		AluOut, Execute_stage_AluOut_OUT, Memory_stage_MemoryData, Memory_stage_MemoryData_OUT, Memory_stage_AluOut_OUT, FileRegisterout0 : in  std_logic_vector(7 downto 0);
		Decode_stage_WRback_OUT, Execute_stage_WRback_OUT, Memory_stage_WRback_OUT                                                        : in  std_logic_vector(2 downto 0);
		Decode_stage_MemOperation_OUT, Execute_stage_MemOperation_OUT, Memory_stage_MemOperation_OUT                                      : in  std_logic_vector(1 downto 0);
		outValue                                                                                                                          : out std_logic_vector(7 downto 0)
	);
end entity ForwardUnit;

architecture RTL of ForwardUnit is
begin
	outValue <= AluOut when Decode_stage_WRback_OUT(1 downto 0) = RegisterNumber and Decode_stage_WRback_OUT(2) = '1' and (Decode_stage_MemOperation_OUT(1) = '1' or Decode_stage_MemOperation_OUT(0) = '0')
		else Execute_stage_AluOut_OUT when Execute_stage_WRback_OUT(1 downto 0) = RegisterNumber and Execute_stage_WRback_OUT(2) = '1' and (Execute_stage_MemOperation_OUT(1) = '1' or Execute_stage_MemOperation_OUT(0) = '0')
		else Memory_stage_MemoryData when Execute_stage_WRback_OUT(1 downto 0) = RegisterNumber and Execute_stage_WRback_OUT(2) = '1' and Execute_stage_MemOperation_OUT(1) = '0' and Execute_stage_MemOperation_OUT(0) = '1'
		else Memory_stage_MemoryData_OUT when Memory_stage_WRback_OUT(1 downto 0) = RegisterNumber and Memory_stage_WRback_OUT(2) = '1' and Memory_stage_MemOperation_OUT(1) = '0' and Memory_stage_MemOperation_OUT(0) = '1'
		else Memory_stage_AluOut_OUT when Memory_stage_WRback_OUT(1 downto 0) = RegisterNumber and Memory_stage_WRback_OUT(2) = '1' and (Memory_stage_MemOperation_OUT(1) = '1' or Memory_stage_MemOperation_OUT(0) = '0')
		else FileRegisterout0;
end architecture RTL;
