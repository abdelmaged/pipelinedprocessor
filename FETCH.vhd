library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_signed.all;
entity fetch is
	port(
		Clk           : in  std_logic;
		rst           : in  std_logic;
		enable        : in  std_logic;
		rd            : in  std_logic;
		Stall         : in  std_logic;
		pc_out        : in  std_logic_vector(7 downto 0);
		pc_out_plus   : out std_logic_vector(7 downto 0);
		ir            : out std_logic_vector(7 downto 0);
		changeFlags   : out std_logic;
		Stall_Next    : out std_logic;
		Rb_instance   : out std_logic_vector(1 downto 0); --- 00 rb=rb || 01 rb is pc || 10 rb is flag register 
		Interrupt     : in  std_logic;
		LoadINterrupt : out std_logic;
		InterruptMask : out std_logic
	);
end entity fetch;

architecture a_fetch of fetch is
	signal temp_ir, EvaluatedIr, PreEvaluatedIr : std_logic_vector(7 downto 0);
	signal B0, B1, B2, B3, B4, B5, B6, B0IN : std_logic_vector(7 downto 0);
	signal OPcode                               : std_logic_vector(5 downto 0);
	signal currentAddress                 : std_logic_vector(7 downto 0);
	component rom is
		port(
			read    : in  std_logic;
			address : in  std_logic_vector(7 downto 0);
			dataout : out std_logic_vector(7 downto 0));
	end component;
	component my_nDFF
		generic(n : integer := 8);
		port(Clk, Rst, enable : in  std_logic;
			 d                : in  std_logic_vector(n - 1 downto 0);
			 q                : out std_logic_vector(n - 1 downto 0));
	end component my_nDFF;

	component my_DFF
		port(d, clk, rst, enable : in  std_logic;
			 q                   : out std_logic);
	end component my_DFF;

	signal int_out, int_out2 : std_logic;
begin
	int_re : my_DFF
		port map(d      => Interrupt,
			     clk    => clk,
			     rst    => rst,
			     enable => enable,
			     q      => int_out);

	int_re2 : my_DFF
		port map(d      => int_out,
			     clk    => clk,
			     rst    => rst,
			     enable => enable,
			     q      => int_out2);

	IR_Buffer0 : my_nDFF generic map(8) port map(Clk, Rst, enable, B0IN, B0);
	IR_Buffer1 : my_nDFF generic map(8) port map(Clk, Rst, enable, B0, B1);
	IR_Buffer2 : my_nDFF generic map(8) port map(Clk, Rst, enable, B1, B2);
	IR_Buffer3 : my_nDFF generic map(8) port map(Clk, Rst, enable, B2, B3);
	IR_Buffer4 : my_nDFF generic map(8) port map(Clk, Rst, enable, B3, B4);
	IR_Buffer5 : my_nDFF generic map(8) port map(Clk, Rst, enable, B4, B5);
	IR_Buffer6 : my_nDFF generic map(8) port map(Clk, Rst, enable, B5, B6);

	currentAddress <= "00000001" when (int_out = '1' and int_out2 = '0')
		else pc_out;
	my_rom : rom port map(rd, currentAddress, temp_ir);
	PreEvaluatedIr <= temp_ir when rst = '0' and Stall = '0'
		else "00000000" when rst = '0' and Stall = '1'
		else "00000000";
	EvaluatedIr <= "11111111" when (int_out = '1' and int_out2 = '0')
		else PreEvaluatedIr;
	OPcode      <= EvaluatedIr(7 downto 2);
	pc_out_plus <= temp_ir when rst = '1' or (int_out = '1' and int_out2 = '0')
		else pc_out + 1;
	process(OPcode, B0, B1, B2, B3, B4, B5, B6, EvaluatedIr) is
	begin
		case B0(7 downto 2) is
			when "110000" | "110001" | "110010" | "110011" =>
				B0IN          <= "00000000";
				ir            <= EvaluatedIr;
				changeFlags   <= '0';
				Stall_Next    <= '0';
				Rb_instance   <= "00";
				LoadINterrupt <= '0';
				InterruptMask <= '0';
			when others =>
				case OPcode is
					when "111111" =>
						B0IN          <= EvaluatedIr;
						ir            <= EvaluatedIr;
						changeFlags   <= '1';
						Stall_Next    <= '0';
						Rb_instance   <= "00";
						LoadINterrupt <= '0';
						InterruptMask <= '1';
					when "110000" | "110001" | "110010" | "110011" =>
						B0IN          <= EvaluatedIr;
						ir            <= EvaluatedIr;
						changeFlags   <= '1';
						Stall_Next    <= '0';
						Rb_instance   <= "00";
						LoadINterrupt <= '0';
						InterruptMask <= '0';
					when "101110" | "101111" =>
						B0IN          <= EvaluatedIr;
						ir            <= "10001011";
						changeFlags   <= '0';
						Stall_Next    <= '1';
						Rb_instance   <= "00";
						LoadINterrupt <= '0';
						InterruptMask <= '0';
					when "101101" =>
						B0IN          <= EvaluatedIr;
						ir            <= "11101100";
						changeFlags   <= '0';
						Stall_Next    <= '1';
						Rb_instance   <= "01";
						LoadINterrupt <= '0';
						InterruptMask <= '0';
					when "011100" =>
						B0IN          <= EvaluatedIr;
						ir            <= "111011" & EvaluatedIr(1 downto 0);
						changeFlags   <= '0';
						Stall_Next    <= '1';
						Rb_instance   <= "00";
						LoadINterrupt <= '0';
						InterruptMask <= '0';
					when "011101" =>
						B0IN          <= EvaluatedIr;
						ir            <= "10001011";
						changeFlags   <= '0';
						Stall_Next    <= '1';
						Rb_instance   <= "00";
						LoadINterrupt <= '0';
						InterruptMask <= '0';
					----LOOP
					when "101000" | "101001" | "101010" | "101011" =>
						B0IN          <= EvaluatedIr;
						ir            <= "100011" & OPcode(1) & OPcode(0);
						changeFlags   <= '0';
						Stall_Next    <= '1';
						Rb_instance   <= "00";
						LoadINterrupt <= '0';
						InterruptMask <= '0';
					when "000000" =>
						case B0(7 downto 2) is
							when "101111" =>
								B0IN          <= EvaluatedIr;
								ir            <= "11011100";
								changeFlags   <= '0';
								Stall_Next    <= '1';
								Rb_instance   <= "10";
								LoadINterrupt <= '0';
								InterruptMask <= '0';
							when "101110" =>
								B0IN          <= EvaluatedIr;
								ir            <= "11011100";
								changeFlags   <= '0';
								Stall_Next    <= '1';
								Rb_instance   <= "01";
								LoadINterrupt <= '0';
								InterruptMask <= '0';
							when "101101" =>
								B0IN          <= EvaluatedIr;
								ir            <= "10001111";
								changeFlags   <= '0';
								Stall_Next    <= '1';
								Rb_instance   <= "00";
								LoadINterrupt <= '0';
								InterruptMask <= '0';
							when "101000" | "101001" | "101010" | "101011" =>
								B0IN          <= EvaluatedIr;
								ir            <= B0;
								changeFlags   <= '0';
								Stall_Next    <= '0';
								Rb_instance   <= "00";
								LoadINterrupt <= '0';
								InterruptMask <= '0';
							---decrement stack pointer 
							when "011100" =>
								B0IN          <= EvaluatedIr;
								ir            <= "10001111";
								changeFlags   <= '0';
								Stall_Next    <= '0';
								Rb_instance   <= "00";
								LoadINterrupt <= '0';
								InterruptMask <= '0';
							when "011101" =>
								B0IN          <= EvaluatedIr;
								ir            <= "110111" & B0(1 downto 0);
								changeFlags   <= '0';
								Stall_Next    <= '1';
								Rb_instance   <= "00";
								LoadINterrupt <= '0';
								InterruptMask <= '0';
							when "000000" =>
								case B1(7 downto 2) is
									when "101111" =>
										B0IN          <= EvaluatedIr;
										ir            <= "10001011";
										changeFlags   <= '0';
										Stall_Next    <= '1';
										Rb_instance   <= "00";
										LoadINterrupt <= '0';
										InterruptMask <= '0';
									when "000000" =>
										case B2(7 downto 2) is
											when "101111" =>
												B0IN          <= EvaluatedIr;
												ir            <= "11011100";
												changeFlags   <= '0';
												Stall_Next    <= '1';
												Rb_instance   <= "01";
												LoadINterrupt <= '0';
												InterruptMask <= '0';
											when "000000" =>
												case B3(7 downto 2) is
													when "000000" =>
														case B4(7 downto 2) is
															when "000000" =>
																case B5(7 downto 2) is
																	when "000000" =>
																		case B6(7 downto 2) is
																			when "101111" =>
																				B0IN          <= EvaluatedIr;
																				ir            <= "00000000";
																				changeFlags   <= '0';
																				Stall_Next    <= '1';
																				Rb_instance   <= "00";
																				LoadINterrupt <= '0';
																				InterruptMask <= '0';
																			when others =>
																				B0IN          <= EvaluatedIr;
																				ir            <= EvaluatedIr;
																				changeFlags   <= '1';
																				Stall_Next    <= '0';
																				Rb_instance   <= "00";
																				LoadINterrupt <= '0';
																				InterruptMask <= '0';
																		end case;
																	when "101111" =>
																		B0IN          <= EvaluatedIr;
																		ir            <= "00000000";
																		changeFlags   <= '0';
																		Stall_Next    <= '1';
																		Rb_instance   <= "00";
																		LoadINterrupt <= '0';
																		InterruptMask <= '0';
																	when others =>
																		B0IN          <= EvaluatedIr;
																		ir            <= EvaluatedIr;
																		changeFlags   <= '1';
																		Stall_Next    <= '0';
																		Rb_instance   <= "00";
																		LoadINterrupt <= '0';
																		InterruptMask <= '0';
																end case;
															when "101111" =>
																B0IN          <= EvaluatedIr;
																ir            <= "00000000";
																changeFlags   <= '0';
																Stall_Next    <= '1';
																Rb_instance   <= "00";
																LoadINterrupt <= '0';
																InterruptMask <= '0';
															when others =>
																B0IN          <= EvaluatedIr;
																ir            <= EvaluatedIr;
																changeFlags   <= '1';
																Stall_Next    <= '0';
																Rb_instance   <= "00";
																LoadINterrupt <= '0';
																InterruptMask <= '0';
														end case;
													when "101110" | "101111" =>
														B0IN          <= EvaluatedIr;
														ir            <= "00000000";
														changeFlags   <= '0';
														Stall_Next    <= '1';
														Rb_instance   <= "00";
														LoadINterrupt <= '0';
														InterruptMask <= '0';
													when others =>
														B0IN          <= EvaluatedIr;
														ir            <= EvaluatedIr;
														changeFlags   <= '1';
														Stall_Next    <= '0';
														Rb_instance   <= "00";
														LoadINterrupt <= '0';
														InterruptMask <= '0';
												end case;
											when "101110" =>
												B0IN        <= EvaluatedIr;
												ir          <= "00000000";
												changeFlags <= '0';
												Stall_Next  <= '1';
												Rb_instance <= "00";
												LoadINterrupt <= '0';
												InterruptMask <= '0';
											when others =>
												B0IN          <= EvaluatedIr;
												ir            <= EvaluatedIr;
												changeFlags   <= '1';
												Stall_Next    <= '0';
												Rb_instance   <= "00";
												LoadINterrupt <= '0';
												InterruptMask <= '0';
										end case;
									when "101110" =>
										B0IN          <= EvaluatedIr;
										ir            <= "00000000";
										changeFlags   <= '0';
										Stall_Next    <= '1';
										Rb_instance   <= "00";
										LoadINterrupt <= '0';
										InterruptMask <= '0';
									when "101101" =>
										B0IN          <= EvaluatedIr;
										ir            <= "101100" & B1(1 downto 0);
										changeFlags   <= '0';
										Stall_Next    <= '0';
										Rb_instance   <= "00";
										LoadINterrupt <= '0';
										InterruptMask <= '0';
									when "011101" =>
										B0IN          <= EvaluatedIr;
										ir            <= "00000000";
										changeFlags   <= '0';
										Stall_Next    <= '0';
										Rb_instance   <= "00";
										LoadINterrupt <= '0';
										InterruptMask <= '0';
									when others =>
										B0IN          <= EvaluatedIr;
										ir            <= EvaluatedIr;
										changeFlags   <= '1';
										Stall_Next    <= '0';
										Rb_instance   <= "00";
										LoadINterrupt <= '0';
										InterruptMask <= '0';
								end case;
							when others =>
								B0IN          <= EvaluatedIr;
								ir            <= EvaluatedIr;
								changeFlags   <= '1';
								Stall_Next    <= '0';
								Rb_instance   <= "00";
								LoadINterrupt <= '0';
								InterruptMask <= '0';
						end case;
					when others =>
						B0IN          <= EvaluatedIr;
						ir            <= EvaluatedIr;
						changeFlags   <= '1';
						Stall_Next    <= '0';
						Rb_instance   <= "00";
						LoadINterrupt <= '0';
						InterruptMask <= '0';
				end case;
		end case;
	end process;
end architecture a_fetch;
