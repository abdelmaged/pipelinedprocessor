library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ControlUnit is
	port(
		Fetch_stage_output          : in  std_logic_vector(7 downto 0);
		Decode_stage_AluMode_OUT    : in  std_logic_vector(5 downto 0);
		changeFlags_Fetch_Stage_OUT : in  std_logic;
		fetch_stage_Request_Stall   : in  std_logic;
		Stall                       : out std_logic;
		AluMode                     : out std_logic_vector(5 downto 0);
		MemOperation                : out std_logic_vector(1 downto 0);
		WRback                      : out std_logic_vector(2 downto 0);
		changeFlags                 : out std_logic
	);
end entity ControlUnit;

architecture RTL of ControlUnit is
	signal opCode        : std_logic_vector(3 downto 0);
	signal ra, rb        : std_logic_vector(1 downto 0);
	signal InternalStall : std_logic;
begin
	--AluMode <= Decode_stage_AluMode_OUT when Fetch_stage_output(7 downto 4) = X"C"
	--	else Fetch_stage_output(7 downto 2);
	AluMode <= "000000" when Decode_stage_AluMode_OUT(5 downto 2) = X"C"
		else Fetch_stage_output(7 downto 2);
	opCode <= X"0" when Decode_stage_AluMode_OUT(5 downto 2) = X"C"
		else Fetch_stage_output(7 downto 4);

	ra <= Fetch_stage_output(3 downto 2);
	rb <= Fetch_stage_output(1 downto 0);

	changeFlags <= changeFlags_Fetch_Stage_OUT;
	Stall       <= InternalStall or fetch_stage_Request_Stall;
	process(opCode, Fetch_stage_output, ra, rb)
	begin
		case opCode is
			--------------------------------------JX --------------------------------------------------------
			when "1001" =>
				WRback        <= "000";
				MemOperation  <= "10";  --nop not sure
				InternalStall <= '0';
			--------------------------------------Nop --------------------------------------------------------
			when "0000" | "1010" =>
				WRback        <= "000";
				MemOperation  <= "10";  --nop not sure
				InternalStall <= '0';
			--------------------------------------mov --------------------------------------------------------
			when "0001" =>
				WRback(2)     <= '1';
				WRback(1)     <= Fetch_stage_output(3);
				WRback(0)     <= Fetch_stage_output(2);
				MemOperation  <= "10";  --nop not sure 
				InternalStall <= '0';
			--------------------------------------add --------------------------------------------------------
			when "0010" =>
				WRback(2)     <= '1';
				WRback(1)     <= Fetch_stage_output(3);
				WRback(0)     <= Fetch_stage_output(2);
				MemOperation  <= "10";  --nop not sure 
				InternalStall <= '0';
			--------------------------------------sub --------------------------------------------------------
			when "0011" =>
				WRback(2)     <= '1';
				WRback(1)     <= Fetch_stage_output(3);
				WRback(0)     <= Fetch_stage_output(2);
				MemOperation  <= "10";  --nop not sure 
				InternalStall <= '0';
			--------------------------------------And --------------------------------------------------------
			when "0100" =>
				WRback(2)     <= '1';
				WRback(1)     <= Fetch_stage_output(3);
				WRback(0)     <= Fetch_stage_output(2);
				MemOperation  <= "10";  --nop not sure 
				InternalStall <= '0';
			--------------------------------------or --------------------------------------------------------
			when "0101" =>
				WRback(2)     <= '1';
				WRback(1)     <= Fetch_stage_output(3);
				WRback(0)     <= Fetch_stage_output(2);
				MemOperation  <= "10";  --nop not sure 
				InternalStall <= '0';
			--------------------------------------IN OUT--------------------------------------------------------
			when X"7" =>
				WRback(2)          <= '1';
				WRback(1 downto 0) <= rb;
				MemOperation       <= "10"; --nop not sure 
				InternalStall      <= '0';
			------------------------------------------------------------------------------------------
			 	when "1000" =>
				WRback(2)    <= '1';
				WRback(1)    <= Fetch_stage_output(1);
				WRback(0)    <= Fetch_stage_output(0);
				MemOperation       <= "10"; --nop not sure 
				InternalStall      <= '0';
-------------------------------------------------------------------------------------------------------				
			----------- RLC, RRC, SETC, CLRC
			when X"6" =>
				WRback(2)          <= not ra(1); -- Write when RLC, RRC, No Write when SETC,CLRC
				WRback(1 downto 0) <= rb;
				MemOperation       <= "10"; --nop not sure	
				InternalStall      <= '0';
			----------------------
			when X"C" =>
				case ra is
					------------------------- LDM R[rb] â imm; ---------------------------
					when "00" =>
						WRback(2)          <= '1';
						WRback(1 downto 0) <= rb;
						MemOperation       <= "10"; --nop not sure
						InternalStall      <= '0';
					------------------------- LDD R[rb] â M[ea]; -------------------------
					when "01" =>
						WRback(2)          <= '1';
						WRback(1 downto 0) <= rb;
						MemOperation       <= "01"; --Read Data and write 
						InternalStall      <= '0';
					------------------------- STD M[ea] â R[rb]; -------------------------
					when "10" =>
						WRback        <= "000";
						MemOperation  <= "11"; --Write Data
						InternalStall <= '0';
					when others => 
						WRback        <= "000";
						MemOperation  <= "10";
						InternalStall <= '0';
				end case;
			------------------------------ LDI R[rb] â M[R[ra]]; -------------------------
			when X"D" =>
				WRback(2)          <= '1';
				WRback(1 downto 0) <= rb;
				MemOperation       <= "01"; --Read Data and write 
				InternalStall      <= '1';
			------------------------------ STI M[R[ra]] âR[rb]; --------------------------
			when X"E" =>
				WRback        <= "000";
				MemOperation  <= "11";  --Write Data
				InternalStall <= '0';
			when others =>
				WRback        <= "000";
				MemOperation  <= "10";
				InternalStall <= '0';
		end case;
	end process;
end architecture RTL;


