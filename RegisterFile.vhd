library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_signed.all;

entity RegesterFile is
	port(
		Clk                              : in  std_logic;
		Rst                              : in  std_logic;
		Wirte                            : in  std_logic;
		writeSelection                   : in  std_logic_vector(1 downto 0);
		writeValue                       : in  std_logic_vector(7 downto 0);
		readSelection_0, readSelection_1 : in  std_logic_vector(1 downto 0);
		readValue_0, readValue_1         : out std_logic_vector(7 downto 0);
		write_sp_enable                  : in  std_logic;
		sp_value : out std_logic_vector(7 downto 0)
	);
end entity RegesterFile;

architecture RTL of RegesterFile is
	component my_nDFF
		generic(n : integer := 8);
		port(Clk, Rst, enable : in  std_logic;
			 d                : in  std_logic_vector(n - 1 downto 0);
			 q                : out std_logic_vector(n - 1 downto 0));
	end component my_nDFF;

	component decoder_2x4
		port(x              : in  std_logic_vector(1 downto 0);
			 e              : in  std_logic;
			 d0, d1, d2, d3 : out std_logic);
	end component decoder_2x4;

	signal q0, q1, q2, q3     : std_logic_vector(7 downto 0);
	signal o0, o1, o2, o3     : std_logic_vector(7 downto 0);
	signal e0, e1, e2, e3     : std_logic;
	signal ee0, ee1, ee2, ee3 : std_logic;
begin
	 
	dec_2x4 : decoder_2x4 port map(writeSelection, '1', e0, e1, e2, e3);
	o0 <= writeValue when rst = '0' else "00000000";
	o1 <= writeValue when rst = '0' else "00000000";
	o2 <= writeValue when rst = '0' else "00000000";
	o3 <= q3-2 when write_sp_enable = '1'
		else writeValue when rst = '0'
		else "11111111";

	ee0 <= (e0 and Wirte) or Rst;
	ee1 <= (e1 and Wirte) or Rst;
	ee2 <= (e2 and Wirte) or Rst;
	ee3 <= (e3 and Wirte) or Rst or write_sp_enable;

	R0 : my_nDFF generic map(8) port map(Clk, '0', ee0, o0, q0);
	R1 : my_nDFF generic map(8) port map(Clk, '0', ee1, o1, q1);
	R2 : my_nDFF generic map(8) port map(Clk, '0', ee2, o2, q2);
	R3 : my_nDFF generic map(8) port map(Clk, '0', ee3, o3, q3);

	readValue_0 <= q0 when readSelection_0 = "00"
		else q1 when readSelection_0 = "01"
		else q2 when readSelection_0 = "10"
		else q3 when readSelection_0 = "11"
		else (others => 'Z');

	readValue_1 <= q0 when readSelection_1 = "00"
		else q1 when readSelection_1 = "01"
		else q2 when readSelection_1 = "10"
		else q3 when readSelection_1 = "11"
		else (others => 'Z');
			
	sp_value <= q3 when write_sp_enable = '1'
	else (others => 'Z'); 

end architecture RTL;
