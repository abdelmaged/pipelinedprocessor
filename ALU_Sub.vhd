library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ALU_Sub is
	port(
		AA, BB   : in  std_logic_vector(7 downto 0); -- ALU Operands
		ZNCV     : out std_logic_vector(3 downto 0); -- Flags: Zero-Negative-Carry-Overflow
		F_out    : out std_logic_vector(7 downto 0)
	);
end entity ALU_Sub;

architecture A_ALU_Sub of ALU_Sub is
	signal temp_cout,carry_in  : std_logic;
	signal temp_Fout,Bhat : std_logic_vector(7 downto 0);
	component my_nadder IS
		Generic(n : integer := 8);
		PORT(
			a, b : in  std_logic_vector(n - 1 downto 0);
			cin  : in  std_logic;
			s    : out std_logic_vector(n - 1 downto 0);
			cout : out std_logic);
	end component;

begin
  carry_in <='1';
  Bhat <= not (BB);
	my_add : my_nadder generic map(8) port map(AA, Bhat, carry_in, temp_Fout, temp_cout);
	-- zero flag
	ZNCV(0) <= '1' when temp_Fout = "00000000"
		else '0';
	-- negative flag
	ZNCV(1) <= temp_Fout(7);            -- not sure from the logic :3 
  -- Carry flag
	ZNCV(2) <= not temp_cout;
	-- over flow flag
	ZNCV(3) <= '1' when (((AA(7) = '0') and (Bhat(7) = '0') and (temp_Fout(7) = '1')) or ((AA(7) = '1') and (Bhat(7) = '1') and (temp_Fout(7) = '0')))
		else '0';	
		F_out   <= temp_Fout;
end architecture A_ALU_Sub;



