library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
Entity ram is
	port(
		clk       : in  std_logic;
		we        : in  std_logic;
		address_1 : in  std_logic_vector(7 downto 0); -- address for CPU Data lowest prority
		address_2 : in  std_logic_vector(7 downto 0); -- address for  Data Interrupt second higher priority
		address_3 : in  std_logic_vector(7 downto 0); -- address for  Data Interrupt  higheest priority
		datain_1  : in  std_logic_vector(7 downto 0); -- Data for CPU  lowest priority
		datain_2  : in  std_logic_vector(7 downto 0); -- Data for Interrupt second higher priority for writing
		datain_3  : in  std_logic_vector(7 downto 0); -- Data for interrupt highest priority for writing
		dataout   : out std_logic_vector(7 downto 0);
		Intsave   : in  std_logic);
end entity ram;

architecture syncrama of ram is
	type ram_type is array (0 to 255) of std_logic_vector(7 downto 0);
	signal ram : ram_type;
begin
	process(clk, we, address_1, ram) is
	Begin
		if we = '0' then
			dataout <= ram(to_integer(unsigned(address_1)));
		elsif rising_edge(clk) then
			if intsave = '0' or (address_1 /= address_2 And (address_1 /= address_3)) then
				ram(to_integer(unsigned(address_1))) <= datain_1;
			end if;

			if intsave = '1' then
				ram(to_integer(unsigned(address_2))) <= datain_2;
				ram(to_integer(unsigned(address_3))) <= datain_3;
			end if;

		end if;
	end process;

end architecture syncrama;


