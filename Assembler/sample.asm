2                 ;the program starts at address 2
125               ;ISR address

;for(int i=0;i<n; i++)
; A[i]++;

;5  ;n=5
;1
;2
;3
;4
;5

LDD R0, 2
LDM R1, 3
LDM R3, 11

LDI R2, R1   ;the begining of the loop @address 11
INC R2
STI R2, R1
INC R1
LOOP R0, R3

.125                ;go to address 125
RTI