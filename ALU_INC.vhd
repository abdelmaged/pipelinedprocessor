library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ALU_Inc is
	port(
		BB    : in  std_logic_vector(7 downto 0); -- ALU Operands
		ZNCV  : out std_logic_vector(3 downto 0); -- Flags: Zero-Negative-Carry-Overflow
		F_out : out std_logic_vector(7 downto 0)
	);
end entity ALU_Inc;

architecture A_ALU_Inc of ALU_Inc is
	signal temp_cout, carry_in : std_logic;
	signal temp_Fout, Ahat     : std_logic_vector(7 downto 0);
	component my_nadder IS
		Generic(n : integer := 8);
		PORT(
			a, b : in  std_logic_vector(n - 1 downto 0);
			cin  : in  std_logic;
			s    : out std_logic_vector(n - 1 downto 0);
			cout : out std_logic);
	end component;

begin
	Ahat     <= (others => '0');
	carry_in <= '1';
	my_add : my_nadder generic map(8) port map(Ahat, BB, carry_in, temp_Fout, temp_cout);
	-- zero flag
	ZNCV(0) <= '1' when temp_Fout = "00000000"
		else '0';
	-- negative flag
	ZNCV(1) <= temp_Fout(7);            -- not sure from the logic :3 
	-- Carry flag
	ZNCV(2) <= temp_cout;
	-- over flow flag
	-- over flow flag
	ZNCV(3) <= '1' when (((Ahat(7) = '0') and (BB(7) = '0') and (temp_Fout(7) = '1')) or ((Ahat(7) = '1') and (BB(7) = '1') and (temp_Fout(7) = '0')))
		else '0';
	F_out <= temp_Fout;
end architecture A_ALU_Inc;







