library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_signed.all;
entity Main is
	port(
		clk      : in  std_logic;
		rst      : in  std_logic;
		INTR     : in  std_logic;
		IN_PORT  : in  std_logic_vector(7 downto 0);
		OUT_PORT : out std_logic_vector(7 downto 0)
	);
end entity Main;

architecture RTL of Main is
	component ForwardUnit
		port(RegisterNumber                                                                                                                    : in  std_logic_vector(1 downto 0);
			 AluOut, Execute_stage_AluOut_OUT, Memory_stage_MemoryData, Memory_stage_MemoryData_OUT, Memory_stage_AluOut_OUT, FileRegisterout0 : in  std_logic_vector(7 downto 0);
			 Decode_stage_WRback_OUT, Execute_stage_WRback_OUT, Memory_stage_WRback_OUT                                                        : in  std_logic_vector(2 downto 0);
			 Decode_stage_MemOperation_OUT, Execute_stage_MemOperation_OUT, Memory_stage_MemOperation_OUT                                      : in  std_logic_vector(1 downto 0);
			 outValue                                                                                                                          : out std_logic_vector(7 downto 0));
	end component ForwardUnit;
	component my_nDFF
		generic(n : integer := 8);
		port(Clk, Rst, enable : in  std_logic;
			 d                : in  std_logic_vector(n - 1 downto 0);
			 q                : out std_logic_vector(n - 1 downto 0));
	end component my_nDFF;
	component fetch
		port(Clk           : in  std_logic;
			 rst           : in  std_logic;
			 enable        : in  std_logic;
			 rd            : in  std_logic;
			 Stall         : in  std_logic;
			 pc_out        : in  std_logic_vector(7 downto 0);
			 pc_out_plus   : out std_logic_vector(7 downto 0);
			 ir            : out std_logic_vector(7 downto 0);
			 changeFlags   : out std_logic;
			 Stall_Next    : out std_logic;
			 Rb_instance   : out std_logic_vector(1 downto 0);
			 Interrupt     : in  std_logic;
			 LoadINterrupt : out std_logic;
			 InterruptMask : out std_logic);
	end component fetch;
	component RegesterFile is
		port(
			Clk                              : in  std_logic;
			Rst                              : in  std_logic;
			Wirte                            : in  std_logic;
			writeSelection                   : in  std_logic_vector(1 downto 0);
			writeValue                       : in  std_logic_vector(7 downto 0);
			readSelection_0, readSelection_1 : in  std_logic_vector(1 downto 0);
			readValue_0, readValue_1         : out std_logic_vector(7 downto 0);
			write_sp_enable                  : in  std_logic;
			sp_value                         : out std_logic_vector(7 downto 0)
		);
	end component;
	component ControlUnit
		port(Fetch_stage_output          : in  std_logic_vector(7 downto 0);
			 Decode_stage_AluMode_OUT    : in  std_logic_vector(5 downto 0);
			 changeFlags_Fetch_Stage_OUT : in  std_logic;
			 fetch_stage_Request_Stall   : in  std_logic;
			 Stall                       : out std_logic;
			 AluMode                     : out std_logic_vector(5 downto 0);
			 MemOperation                : out std_logic_vector(1 downto 0);
			 WRback                      : out std_logic_vector(2 downto 0);
			 changeFlags                 : out std_logic);
	end component ControlUnit;

	component ALU is
		port(
			A, B              : in  std_logic_vector(7 downto 0);
			MemoryAddreesIn   : in  std_logic_vector(7 downto 0);
			CCR_IN            : in  std_logic_vector(3 downto 0);
			AluMode           : in  std_logic_vector(5 downto 0);
			IN_PORT           : in  std_logic_vector(7 downto 0);
			SecondByte        : in  std_logic_vector(7 downto 0);
			outputPort        : out std_logic_vector(7 downto 0);
			outputPort_Enable : out std_logic;
			Y                 : out std_logic_vector(7 downto 0);
			MemoryAddreesOut  : out std_logic_vector(7 downto 0);
			MemoryDataout     : out std_logic_vector(7 downto 0);
			CCR_Out           : out std_logic_vector(3 downto 0)
		);
	end component;
	component ram
		port(clk       : in  std_logic;
			 we        : in  std_logic;
			 address_1 : in  std_logic_vector(7 downto 0);
			 address_2 : in  std_logic_vector(7 downto 0);
			 address_3 : in  std_logic_vector(7 downto 0);
			 datain_1  : in  std_logic_vector(7 downto 0);
			 datain_2  : in  std_logic_vector(7 downto 0);
			 datain_3  : in  std_logic_vector(7 downto 0);
			 dataout   : out std_logic_vector(7 downto 0);
			 Intsave   : in  std_logic);
	end component ram;
	--Signals
	signal Fetch_stage_input, Fetch_stage_output                                                                                                      : std_logic_vector(7 downto 0);
	signal PCin, PCOut, FetchPcOut, Fecth_stage_pc_out, InputsToPc                                                                                    : std_logic_vector(7 downto 0);
	signal CCR_IN, CCR_OUT, InputsToCCR                                                                                                               : std_logic_vector(3 downto 0);
	signal CCR_enable, LoadPcFromMemory, LoadflagsFromMemory                                                                                          : std_logic;
	signal FileRegisterout0, FileRegisterout1, Decode_stage_out0_OUT, Decode_stage_out1_OUT, Decode_stage_out1_PreValue, out0, out1, RB, RA, sp_value : std_logic_vector(7 downto 0);
	signal AluMode, Decode_stage_AluMode_OUT                                                                                                          : std_logic_vector(5 downto 0);
	signal MemOperation, Decode_stage_MemOperation_OUT, Execute_stage_MemOperation_OUT, Memory_stage_MemOperation_OUT                                 : std_logic_vector(1 downto 0);
	signal WRback, Decode_stage_WRback_OUT, Execute_stage_WRback_OUT                                                                                  : std_logic_vector(2 downto 0);
	signal AluOut, Execute_stage_AluOut_OUT                                                                                                           : std_logic_vector(7 downto 0);
	signal Execute_stage_MemoryAdress, Execute_stage_MemoryAdress_OUT, Execute_stage_MemoryAdress_Alu_OUT                                             : std_logic_vector(7 downto 0);
	signal Memory_stage_MemoryData, Memory_stage_MemoryData_OUT, Memory_stage_AluOut_OUT                                                              : std_logic_vector(7 downto 0);
	signal Execute_stage_MemoryData_Alu_OUT, Execute_stage_MemoryData_OUT, Execute_stage_IN_Port, Decode_stage_IN_Port                                : std_logic_vector(7 downto 0);
	signal MemoryWrite                                                                                                                                : std_logic;
	signal Memory_stage_WRback_OUT                                                                                                                    : std_logic_vector(2 downto 0);
	signal writeValue                                                                                                                                 : std_logic_vector(7 downto 0);
	signal Write_Back_Enable                                                                                                                          : std_logic;
	signal ControlUnit_Stall_out                                                                                                                      : std_logic;
	signal outputPort                                                                                                                                 : std_logic_vector(7 downto 0);
	signal outputPort_Enable                                                                                                                          : std_logic;
	signal Decode_stage_changeFlags_OUT, ControlUnit_changeFlags_OUT                                                                                  : std_logic_vector(0 downto 0);
	signal Fetch_stage_changeFlags_in, fetch_stage_Request_Stall_in, Fetch_stage_changeFlags_out, fetch_stage_Request_Stall_out                       : std_logic_vector(0 downto 0);
	signal Rb_instance, fetch_stage_Rb_Rb_instance                                                                                                    : std_logic_vector(1 downto 0);
	signal Decode_stage_Rb_Rb_instance, Execute_stage_Rb_Rb_instance, memory_stage_Rb_Rb_instance                                                     : std_logic_vector(1 downto 0);
	signal PC_IS_RB                                                                                                                                   : std_logic;
	signal InterruptMask                                                                                                                              : std_logic;
	signal LoadINterrupt, LoadINterruptout                                                                                                            : std_logic_vector(0 downto 0);
	signal enable   : std_logic;
begin
	enable <= '1';
	--Fetch
	-- fetch_lbl : fetch port map(clk, rst, enable, '1', ControlUnit_Stall_out, FetchPcOut, Fecth_stage_pc_out, Fetch_stage_input, Fetch_stage_changeFlags_in(0), fetch_stage_Request_Stall_in(0), Rb_instance, INTR, LoadINterrupt(0), InterruptMask);
	FETCH_lbl : fetch
		port map(Clk           => Clk,
			     rst           => rst,
			     enable        => enable,
			     rd            => '1',
			     Stall         => ControlUnit_Stall_out,
			     pc_out        => FetchPcOut,
			     pc_out_plus   => Fecth_stage_pc_out,
			     ir            => Fetch_stage_input,
			     changeFlags   => Fetch_stage_changeFlags_in(0),
			     Stall_Next    => fetch_stage_Request_Stall_in(0),
			     Rb_instance   => Rb_instance,
			     Interrupt     => INTR,
			     LoadINterrupt => LoadINterrupt(0),
			     InterruptMask => InterruptMask);

	Fetch_stage : my_nDFF generic map(8) port map(Clk, Rst, enable, Fetch_stage_input, Fetch_stage_output);
	Fetch_Request_Stall : my_nDFF generic map(1) port map(Clk, Rst, enable, fetch_stage_Request_Stall_in, fetch_stage_Request_Stall_out);
	Fetch_ChangeFlags : my_nDFF generic map(1) port map(Clk, Rst, enable, Fetch_stage_changeFlags_in, Fetch_stage_changeFlags_out);
	Fetch_Rb_Is_Pc : my_nDFF generic map(2) port map(Clk, Rst, enable, Rb_instance, fetch_stage_Rb_Rb_instance);
	LoadINterrupt_out : my_nDFF generic map(1) port map(Clk, Rst, enable, LoadINterrupt, LoadINterruptout);
	-----------------Decode Stage Registers ---------------------
	Decode_stage_out0 : my_nDFF generic map(8) port map(Clk, Rst, enable, out0, Decode_stage_out0_OUT);
	Decode_stage_out1 : my_nDFF generic map(8) port map(Clk, Rst, enable, out1, Decode_stage_out1_PreValue);
	Decode_stage_AluMode : my_nDFF generic map(6) port map(Clk, Rst, enable, AluMode, Decode_stage_AluMode_OUT);
	Decode_stage_MemOperation : my_nDFF generic map(2) port map(Clk, Rst, enable, MemOperation, Decode_stage_MemOperation_OUT);
	Decode_stage_WRback : my_nDFF generic map(3) port map(Clk, Rst, enable, WRback, Decode_stage_WRback_OUT);
	Decode_stage_changeFlags : my_nDFF generic map(1) port map(Clk, Rst, enable, ControlUnit_changeFlags_OUT, Decode_stage_changeFlags_OUT);
	Decode_Rb_Is_Pc : my_nDFF generic map(2) port map(Clk, Rst, enable, fetch_stage_Rb_Rb_instance, Decode_stage_Rb_Rb_instance);
	Decode_IN_PORT : my_nDFF generic map(8) port map(Clk, Rst, enable, IN_PORT, Decode_stage_IN_Port);
	-------------------------------------------------------------
	-------------------Execution---------------------------------
	Execute_stage_AR : my_nDFF generic map(8) port map(Clk, Rst, enable, AluOut, Execute_stage_AluOut_OUT);
	Execute_stage_MemOperation : my_nDFF generic map(2) port map(Clk, Rst, enable, Decode_stage_MemOperation_OUT, Execute_stage_MemOperation_OUT);
	Execute_stage_WRback : my_nDFF generic map(3) port map(Clk, Rst, enable, Decode_stage_WRback_OUT, Execute_stage_WRback_OUT);
	Execute_stage_MemoryAdress_lbl : my_nDFF generic map(8) port map(Clk, Rst, enable, Execute_stage_MemoryAdress_Alu_OUT, Execute_stage_MemoryAdress_OUT);
	Execute_stage_MmeoryData_lbl : my_nDFF generic map(8) port map(Clk, Rst, enable, Execute_stage_MemoryData_Alu_OUT, Execute_stage_MemoryData_OUT);
	Execute_Rb_Is_Pc : my_nDFF generic map(2) port map(Clk, Rst, enable, Decode_stage_Rb_Rb_instance, Execute_stage_Rb_Rb_instance);
	Execute_IN_PORT : my_nDFF generic map(8) port map(Clk, Rst, enable, Decode_stage_IN_Port, Execute_stage_IN_Port);

	Execute_stage_MemoryAdress <= X"00";
	Decode_stage_out1_out      <= "0000" & CCR_OUT when Decode_stage_Rb_Rb_instance = "10"
		else Decode_stage_out1_PreValue;

	--	Alu_Mode_Eval <= "000000" when InterruptMask = '1'
	--		else Decode_stage_AluMode_OUT;

	ALU_lbl : ALU
		port map(A                 => Decode_stage_out0_OUT,
			     B                 => Decode_stage_out1_OUT,
			     MemoryAddreesIn   => Execute_stage_MemoryAdress,
			     CCR_IN            => CCR_OUT,
			     AluMode           => Decode_stage_AluMode_OUT,
			     IN_PORT           => Execute_stage_IN_Port,
			     SecondByte        => Fetch_stage_output,
			     outputPort        => outputPort,
			     outputPort_Enable => outputPort_Enable,
			     Y                 => AluOut,
			     MemoryAddreesOut  => Execute_stage_MemoryAdress_Alu_OUT,
			     MemoryDataout     => Execute_stage_MemoryData_Alu_OUT,
			     CCR_Out           => CCR_IN);
	----------------Memory Stage -------------------------------- -------------------------------------------------------------

	----------------Memory Stage -------------------------------- 
	MemoryWrite <= (Execute_stage_MemOperation_OUT(1) and Execute_stage_MemOperation_OUT(0)) or InterruptMask;
	--	RAM_lbl : ram port map(Clk, MemoryWrite, Execute_stage_MemoryAdress_OUT, Execute_stage_MemoryData_OUT, Memory_stage_MemoryData);
	RAM_lbl : ram
		port map(clk       => clk,
			     we        => MemoryWrite,
			     address_1 => Execute_stage_MemoryAdress_OUT,
			     address_2 => sp_value,
			     address_3 => sp_value - 1,
			     datain_1  => Execute_stage_MemoryData_OUT,
			     datain_2  => FetchPcOut,
			     datain_3  => X"0" & CCR_OUT,
			     dataout   => Memory_stage_MemoryData,
			     Intsave   => InterruptMask);

	Memory_stage_AR : my_nDFF generic map(8) port map(Clk, Rst, enable, Execute_stage_AluOut_OUT, Memory_stage_AluOut_OUT);
	Memory_stage_Data : my_nDFF generic map(8) port map(Clk, Rst, enable, Memory_stage_MemoryData, Memory_stage_MemoryData_OUT);
	Memory_stage_WRback : my_nDFF generic map(3) port map(Clk, Rst, enable, Execute_stage_WRback_OUT, Memory_stage_WRback_OUT);
	Memory_stage_MemOperation : my_nDFF generic map(2) port map(Clk, Rst, enable, Execute_stage_MemOperation_OUT, Memory_stage_MemOperation_OUT);
	Memory_stage_Rb_Is_Pc : my_nDFF generic map(2) port map(Clk, Rst, enable, Execute_stage_Rb_Rb_instance, memory_stage_Rb_Rb_instance);
	LoadPcFromMemory <= '1' when Execute_stage_MemOperation_OUT = "01" and Execute_stage_Rb_Rb_instance = "01"
		else '0';
	LoadFlagsFromMemory <= '1' when Execute_stage_MemOperation_OUT = "01" and Execute_stage_Rb_Rb_instance = "10"
		else '0';
	-------------------------------------------------------------
	--Registers
	PC_IS_RB <= '1' when   (Fetch_stage_output(7 downto 4) = "1010"   and out0 /= X"00") 
						or (Fetch_stage_output(7 downto 2) = "100100" and CCR_IN(0) = '1') 
						or (Fetch_stage_output(7 downto 2) = "100101" and CCR_IN(1) = '1') 
						or (Fetch_stage_output(7 downto 2) = "100110" and CCR_IN(2) = '1') 
						or (Fetch_stage_output(7 downto 2) = "100111" and CCR_IN(3) = '1') 
						or (Fetch_stage_output(7 downto 2) = "101100")
		else '0';
	pcin <= PCOut when ControlUnit_Stall_out = '1'
		else Fecth_stage_pc_out;
	FetchPcOut <= out1 when PC_IS_RB = '1'
		else PCOut;
	InputsToPc <= pcin when LoadPcFromMemory = '0'
		else Memory_stage_MemoryData;
	PC_reg : my_nDFF generic map(8) port map(Clk, '0', enable, InputsToPc, PCOut);
	CCR_enable <= Decode_stage_changeFlags_OUT(0) or LoadFlagsFromMemory;

	InputsToCCR <= CCR_IN when LoadflagsFromMemory = '0'
		else Memory_stage_MemoryData(3 downto 0);
	CCR_reg : my_nDFF generic map(4) port map(Clk, Rst, CCR_enable, InputsToCCR, CCR_OUT);
	OutputPort_lbl : my_nDFF generic map(8) port map(Clk, Rst, outputPort_Enable, outputPort, OUT_PORT);

	--Control unit & Decode Stage logic layer
	CU_lbl : ControlUnit port map(Fetch_stage_output, Decode_stage_AluMode_OUT, Fetch_stage_changeFlags_out(0), fetch_stage_Request_Stall_out(0), ControlUnit_Stall_out, AluMode, MemOperation, WRback, ControlUnit_changeFlags_OUT(0));

	--RegesterFile
	Write_Back_Enable <= (Memory_stage_WRback_OUT(2) and (not memory_stage_Rb_Rb_instance(0)) and (not memory_stage_Rb_Rb_instance(1)));
	writeValue        <= Memory_stage_MemoryData_OUT when Memory_stage_MemOperation_OUT(1) = '0' and Memory_stage_MemOperation_OUT(0) = '1'
		else Memory_stage_AluOut_OUT;
	-- DataForward
	-- we `ll forward form the memory when it `ll write its databack
	-- if not we `ll forward from the execution stage 
	-- if not we `ll Take data from the file register
	out0_Forward : component ForwardUnit
		port map(
			RegisterNumber                 => Fetch_stage_output(3 downto 2),
			AluOut                         => AluOut,
			Execute_stage_AluOut_OUT       => Execute_stage_AluOut_OUT,
			Memory_stage_MemoryData        => Memory_stage_MemoryData,
			Memory_stage_MemoryData_OUT    => Memory_stage_MemoryData_OUT,
			Memory_stage_AluOut_OUT        => Memory_stage_AluOut_OUT,
			FileRegisterout0               => FileRegisterout0,
			Decode_stage_WRback_OUT        => Decode_stage_WRback_OUT,
			Execute_stage_WRback_OUT       => Execute_stage_WRback_OUT,
			Memory_stage_WRback_OUT        => Memory_stage_WRback_OUT,
			Decode_stage_MemOperation_OUT  => Decode_stage_MemOperation_OUT,
			Execute_stage_MemOperation_OUT => Execute_stage_MemOperation_OUT,
			Memory_stage_MemOperation_OUT  => Memory_stage_MemOperation_OUT,
			outValue                       => RA
		);
	out1_Forward : component ForwardUnit
		port map(
			RegisterNumber                 => Fetch_stage_output(1 downto 0),
			AluOut                         => AluOut,
			Execute_stage_AluOut_OUT       => Execute_stage_AluOut_OUT,
			Memory_stage_MemoryData        => Memory_stage_MemoryData,
			Memory_stage_MemoryData_OUT    => Memory_stage_MemoryData_OUT,
			Memory_stage_AluOut_OUT        => Memory_stage_AluOut_OUT,
			FileRegisterout0               => FileRegisterout1,
			Decode_stage_WRback_OUT        => Decode_stage_WRback_OUT,
			Execute_stage_WRback_OUT       => Execute_stage_WRback_OUT,
			Memory_stage_WRback_OUT        => Memory_stage_WRback_OUT,
			Decode_stage_MemOperation_OUT  => Decode_stage_MemOperation_OUT,
			Execute_stage_MemOperation_OUT => Execute_stage_MemOperation_OUT,
			Memory_stage_MemOperation_OUT  => Memory_stage_MemOperation_OUT,
			outValue                       => RB
		);
	out0 <= "00000001" when LoadINterruptout(0) = '1'
		else RA;
	out1 <= pcin when fetch_stage_Rb_Rb_instance = "01"
		else "0000" & CCR_OUT when fetch_stage_Rb_Rb_instance = "10"
		else RB;

	-- RegesterFile_lbl : RegesterFile port map(Clk, Rst, Write_Back_Enable, Memory_stage_WRback_OUT(1 downto 0), writeValue, Fetch_stage_output(3 downto 2), Fetch_stage_output(1 downto 0), FileRegisterout0, FileRegisterout1);


	RF_lbl : RegesterFile
		port map(Clk             => Clk,
			     Rst             => Rst,
			     Wirte           => Write_Back_Enable,
			     writeSelection  => Memory_stage_WRback_OUT(1 downto 0),
			     writeValue      => writeValue,
			     readSelection_0 => Fetch_stage_output(3 downto 2),
			     readSelection_1 => Fetch_stage_output(1 downto 0),
			     readValue_0     => FileRegisterout0,
			     readValue_1     => FileRegisterout1,
			     write_sp_enable => InterruptMask,
			     sp_value        => sp_value);
end architecture RTL;
