library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ALU is
	port(
		A, B              : in  std_logic_vector(7 downto 0); -- ALU Operands
		MemoryAddreesIn   : in  std_logic_vector(7 downto 0);
		CCR_IN            : in  std_logic_vector(3 downto 0); -- Flags: Zero-Negative-Carry-Overflow
		AluMode           : in  std_logic_vector(5 downto 0);
		IN_PORT           : in  std_logic_vector(7 downto 0);
		SecondByte        : in  std_logic_vector(7 downto 0);
		Y                 : out std_logic_vector(7 downto 0); -- ALU Result
		MemoryAddreesOut  : out std_logic_vector(7 downto 0);
		MemoryDataout     : out std_logic_vector(7 downto 0);
		outputPort        : out std_logic_vector(7 downto 0);
		outputPort_Enable : out std_logic;
		CCR_Out           : out std_logic_vector(3 downto 0)
	);
end entity ALU;

architecture RTL of ALU is
	signal opCode                             : std_logic_vector(3 downto 0);
	signal ra                                 : std_logic_vector(1 downto 0);
	signal Z, N, C, V                         : std_logic; -- Flags: Zero-Negative-Carry-Overflow
	signal F                                  : std_logic_vector(7 downto 0); -- alu adder result
	signal adder_flaags                       : std_logic_vector(3 downto 0);
	signal sub_flaags                         : std_logic_vector(3 downto 0);
	signal FS                                 : std_logic_vector(7 downto 0); -- alu sub result
	signal FA                                 : std_logic_vector(7 downto 0); -- alu and result
	signal FO                                 : std_logic_vector(7 downto 0); -- alu or result
	signal FNot, FNeg, FDec, FInc             : std_logic_vector(7 downto 0);
	signal Dec_flaags, Inc_flaags, Neg_flaags : std_logic_vector(3 downto 0);
	component ALU_ADDER is
		port(
			AA, BB   : in  std_logic_vector(7 downto 0); -- ALU Operands
			carry_in : in  std_logic;
			ZNCV     : out std_logic_vector(3 downto 0); -- Flags: Zero-Negative-Carry-Overflow
			F_out    : out std_logic_vector(7 downto 0)
		);
	end component;
	component ALU_Sub is
		port(
			AA, BB : in  std_logic_vector(7 downto 0); -- ALU Operands
			ZNCV   : out std_logic_vector(3 downto 0); -- Flags: Zero-Negative-Carry-Overflow
			F_out  : out std_logic_vector(7 downto 0)
		);
	end component;
	component ALU_Dec is
		port(
			BB    : in  std_logic_vector(7 downto 0); -- ALU Operands
			ZNCV  : out std_logic_vector(3 downto 0); -- Flags: Zero-Negative-Carry-Overflow
			F_out : out std_logic_vector(7 downto 0)
		);
	end component;
	component ALU_Inc is
		port(
			BB    : in  std_logic_vector(7 downto 0); -- ALU Operands
			ZNCV  : out std_logic_vector(3 downto 0); -- Flags: Zero-Negative-Carry-Overflow
			F_out : out std_logic_vector(7 downto 0)
		);
	end component;
	component ALU_Neg is
		port(
			BB       : in  std_logic_vector(7 downto 0); -- ALU Operands
			flag_old : in  std_logic_vector(3 downto 0); -- Flags: Zero-Negative-Carry-Overflow
			ZNCV     : out std_logic_vector(3 downto 0); -- Flags: Zero-Negative-Carry-Overflow
			F_out    : out std_logic_vector(7 downto 0)
		);
	end component;
begin
	-- Extract Signals
	opCode <= AluMode(5 downto 2);
	ra     <= AluMode(1 downto 0);

	My_ALU_ADDER : ALU_ADDER port map(A, B, CCR_IN(2), adder_flaags, F);
	My_ALU_Sub : ALU_Sub port map(A, B, Sub_flaags, FS);
	MY_ALU_AND : FA   <= A and B;
	MY_ALU_OR : FO    <= A or B;
	My_ALU_Not : FNot <= not (B);
	My_ALU_Dec : ALU_Dec port map(B, Dec_flaags, FDec);
	My_ALU_Inc : ALU_Inc port map(B, Inc_flaags, FInc);
	My_ALU_Neg : ALU_Neg port map(B, CCR_IN, Neg_flaags, FNeg);
	-- Main Process
	process(opCode, ra, CCR_IN, A, B, MemoryAddreesIn, F, FS, FO, FA, FDec, FInc, FNot, FNeg, sub_flaags, adder_flaags, Dec_flaags, IN_PORT, Inc_flaags, Neg_flaags, SecondByte) is
	begin
		case opCode is
			when X"7" =>
				case ra is
					--------------------------------------OUT------------------------------------------------
					when "10" =>
						outputPort_Enable <= '1';
						outputPort        <= B;
						Y                 <= B;
					--------------------------------------IN------------------------------------------------
					when "11" =>
						Y                 <= IN_PORT;
						outputPort        <= X"00";
						outputPort_Enable <= '0';
					when others =>
						Y                 <= X"00";
						outputPort        <= X"00";
						outputPort_Enable <= '0';
				end case;
				Z                <= CCR_IN(0);
				N                <= CCR_IN(1);
				C                <= CCR_IN(2);
				V                <= CCR_IN(3);
				MemoryAddreesOut <= MemoryAddreesIn;
				MemoryDataout    <= X"00";
			------------------------------------------------------------------------------------------
			when X"6" =>
				case ra is
					--------------------------------------RLC C?R[rb]<7>; R[rb]?R[rb]<6:0>&C;-----------------
					when "00" =>
						C <= B(7);
						Y <= B(6 downto 0) & CCR_IN(2);
					--------------------------------------RRC C?R[rb]<0>; R[rb]?C&R[rb]<7:1>;-----------------
					when "01" =>
						C <= B(0);
						Y <= CCR_IN(2) & B(7 downto 1);
					--------------------------------------SETC------------------------------------------------
					when "10" =>
						C <= '1';
						Y <= X"00";
					--------------------------------------CLRC------------------------------------------------
					when "11" =>
						C <= '0';
						Y <= X"00";
					when others =>
						C <= CCR_IN(2);
						Y <= X"00";
				end case;
				Z                 <= CCR_IN(0);
				N                 <= CCR_IN(1);
				V                 <= CCR_IN(3);
				MemoryAddreesOut  <= MemoryAddreesIn;
				MemoryDataout     <= X"00";
				outputPort_Enable <= '0';
				outputPort        <= X"00";
			when X"C" =>
				------------------------- LDM R[rb] ← imm; ---------------------------
				------------------------- LDD R[rb] ← M[ea]; -------------------------
				------------------------- STD M[ea] ← R[rb]; -------------------------
				Y             <= SecondByte;
				MemoryDataout <= B;
				if (ra /= "00") then
					MemoryAddreesOut <= SecondByte;
				else
					MemoryAddreesOut <= MemoryAddreesIn;
				end if;
				--				MemoryAddreesOut <= SecondByte when ra /= "00"
				--					else MemoryAddreesIn;
				Z                 <= CCR_IN(0);
				N                 <= CCR_IN(1);
				C                 <= CCR_IN(2);
				V                 <= CCR_IN(3);
				outputPort_Enable <= '0';
				outputPort        <= X"00";
			------------------------------ LDI R[rb] ← M[R[ra]]; -------------------------
			when X"D" =>
				Y                 <= SecondByte;
				MemoryDataout     <= B;
				MemoryAddreesOut  <= A;
				Z                 <= CCR_IN(0);
				N                 <= CCR_IN(1);
				C                 <= CCR_IN(2);
				V                 <= CCR_IN(3);
				outputPort_Enable <= '0';
				outputPort        <= X"00";
			------------------------------ STI M[R[ra]] ←R[rb]; --------------------------
			when X"E" =>
				Y                 <= X"00";
				MemoryDataout     <= B;
				MemoryAddreesOut  <= A;
				Z                 <= CCR_IN(0);
				N                 <= CCR_IN(1);
				C                 <= CCR_IN(2);
				V                 <= CCR_IN(3);
				outputPort_Enable <= '0';
				outputPort        <= X"00";
			--------------------------------------Nop--------------------------------------------------------
			when "0000" =>
				Y                 <= X"00"; -- no alu operation 
				Z                 <= CCR_IN(0); -- ytl3 el flags el admea till now
				N                 <= CCR_IN(1);
				C                 <= CCR_IN(2);
				V                 <= CCR_IN(3);
				MemoryAddreesOut  <= MemoryAddreesIn; -- m4 b8yr 7aga fe el memory address
				MemoryDataout     <= X"00"; -- m4 bsave ay 7aga fe el memory data 
				outputPort_Enable <= '0';
				outputPort        <= X"00";
			-------------------------------------MOV R[ra]=R[rb] ---------------------------------------------	
			when "0001" =>
				Y                 <= B; -- save rb in ra  
				Z                 <= CCR_IN(0); -- ytl3 el flags el adema till now
				N                 <= CCR_IN(1);
				C                 <= CCR_IN(2);
				V                 <= CCR_IN(3);
				MemoryAddreesOut  <= MemoryAddreesIn;
				MemoryDataout     <= X"00";
				outputPort_Enable <= '0';
				outputPort        <= X"00";
			---------------------------------------ADD R[ra]=R[ra]+R[rb]---------------------------------------
			when "0010" =>
				Y                 <= F; -- alu output
				Z                 <= adder_flaags(0); -- ubdate the flags
				N                 <= adder_flaags(1);
				C                 <= adder_flaags(2);
				V                 <= adder_flaags(3);
				MemoryAddreesOut  <= MemoryAddreesIn;
				MemoryDataout     <= X"00";
				outputPort_Enable <= '0';
				outputPort        <= X"00";
			---------------------------------------sub R[ra]=R[ra]+R[rb]---------------------------------------
			when "0011" =>
				Y                 <= FS; -- alu output
				Z                 <= sub_flaags(0); -- ubdate the flags
				N                 <= sub_flaags(1);
				C                 <= sub_flaags(2);
				V                 <= sub_flaags(3);
				MemoryAddreesOut  <= MemoryAddreesIn;
				MemoryDataout     <= X"00";
				outputPort_Enable <= '0';
				outputPort        <= X"00";
			---------------------------------------AND---------------------------------------
			when "0100" =>
				Y <= FA;                -- alu output
				---Z checker  --
				case FA is
					when "00000000" =>
						Z <= '1';
					when others =>
						Z <= '0';
				end case;
				-----------checker end
				N                 <= FA(7);
				C                 <= CCR_IN(2);
				V                 <= CCR_IN(3);
				MemoryAddreesOut  <= MemoryAddreesIn;
				MemoryDataout     <= X"00";
				outputPort_Enable <= '0';
				outputPort        <= X"00";
			---------------------------------------OR---------------------------------------
			when "0101" =>
				Y <= FO;                -- alu output
				---Z checker  --
				case FO is
					when "00000000" =>
						Z <= '1';
					when others =>
						Z <= '0';
				end case;
				N                 <= FO(7);
				C                 <= CCR_IN(2);
				V                 <= CCR_IN(3);
				MemoryAddreesOut  <= MemoryAddreesIn;
				MemoryDataout     <= X"00";
				outputPort_Enable <= '0';
				outputPort        <= X"00";
			---------------------------------------Not-Neg-Dec-Inc----------------------------------------
			when "1000" =>
				outputPort_Enable <= '0';
				outputPort        <= X"00";
				MemoryAddreesOut  <= MemoryAddreesIn;
				MemoryDataout     <= X"00";
				case ra is
					--------------------------------------R[rb]=1's Complement(R[rb])-----------------
					when "00" =>
						Y <= FNot;      -- alu output
						---Z checker  --
						case FNot is
							when "00000000" =>
								Z <= '1';
							when others =>
								Z <= '0';
						end case;
						N <= FNot(7);
						C <= CCR_IN(2);
						V <= CCR_IN(3);
					--------------------------------------R[rb]=2?s Complement(R[rb])-----------------
					when "01" =>
						Y <= FNeg;      -- alu output
						Z <= Neg_flaags(0); -- ubdate the flags
						N <= Neg_flaags(1);
						C <= Neg_flaags(2);
						V <= Neg_flaags(3);
					--------------------------------------R[rb]=R[rb] + 1------------------------------------------------
					when "10" =>
						Y <= FInc;      -- alu output
						Z <= Inc_flaags(0); -- ubdate the flags
						N <= Inc_flaags(1);
						C <= Inc_flaags(2);
						V <= Inc_flaags(3);
					--------------------------------------R[rb]=R[rb] - 1------------------------------------------------
					when "11" =>
						Y <= FDec;      -- alu output
						Z <= Dec_flaags(0); -- ubdate the flags
						N <= Dec_flaags(1);
						C <= Dec_flaags(2);
						V <= Dec_flaags(3);
					-----------------------------------------------------------------------------------------------						
					when others =>      --not sure
						Y <= X"00";
						Z <= CCR_IN(0);
						N <= CCR_IN(1);
						C <= CCR_IN(2);
						V <= CCR_IN(3);
				end case;
			-------------------------------------------------------------------------------------------------------				
			when others =>
				Y                 <= X"00";
				Z                 <= CCR_IN(0);
				N                 <= CCR_IN(1);
				C                 <= CCR_IN(2);
				V                 <= CCR_IN(3);
				MemoryAddreesOut  <= MemoryAddreesIn;
				MemoryDataout     <= X"00";
				outputPort_Enable <= '0';
				outputPort        <= X"00";
		end case;
	end process;

	CCR_Out <= V & C & N & Z;
end architecture RTL;



