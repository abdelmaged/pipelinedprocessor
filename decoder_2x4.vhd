library ieee;
use ieee.std_logic_1164.all;

entity decoder_2x4 is
	port(	
		x : in std_logic_vector(1 downto 0);
		e : in std_logic;
		d0,d1,d2,d3 : out std_logic
	);
end decoder_2x4;

architecture a_decoder_2x4 of decoder_2x4 is
begin
	d0 <= '1' when e = '1' and x(1) = '0' and x(0) = '0' else '0';
	d1 <= '1' when e = '1' and x(1) = '0' and x(0) = '1' else '0';
	d2 <= '1' when e = '1' and x(1) = '1' and x(0) = '0' else '0';
	d3 <= '1' when e = '1' and x(1) = '1' and x(0) = '1' else '0';
end a_decoder_2x4;