
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ALU_Neg is
	port(
		BB       : in  std_logic_vector(7 downto 0); -- ALU Operands
		flag_old : in std_logic_vector(3 downto 0); -- Flags: Zero-Negative-Carry-Overflow
		ZNCV     : out std_logic_vector(3 downto 0); -- Flags: Zero-Negative-Carry-Overflow
		F_out    : out std_logic_vector(7 downto 0)
	);
end entity ALU_Neg;

architecture A_ALU_Neg of ALU_Neg is
	signal temp_cout ,carry_in  : std_logic;
	signal temp_Fout,Ahat,Bhat   : std_logic_vector(7 downto 0);
	component my_nadder IS
		Generic(n : integer := 8);
		PORT(
			a, b : in  std_logic_vector(n - 1 downto 0);
			cin  : in  std_logic;
			s    : out std_logic_vector(n - 1 downto 0);
			cout : out std_logic);
	end component;

begin
   Ahat             <= (others=>'0');		      
	 Bhat             <=  not(BB);
	 carry_in         <='1';
	my_add : my_nadder generic map(8) port map(Ahat, Bhat, carry_in, temp_Fout, temp_cout);
	-- zero flag
	ZNCV(0) <= '1' when temp_Fout = "00000000"
		else '0';
	-- negative flag
	ZNCV(1) <= temp_Fout(7);            
  -- Carry flag
	ZNCV(2) <= flag_old(2);
	-- over flow flag
	ZNCV(3) <= flag_old(3);
	
		F_out   <= temp_Fout;
end architecture A_ALU_Neg;








