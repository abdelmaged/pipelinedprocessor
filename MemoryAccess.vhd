Library ieee;
Use ieee.std_logic_1164.all;
entity MemoryS is
	port(
		clk            : in  std_logic;
		opcode         : in  std_logic_vector(3 downto 0);
		ra             : in  std_logic_vector(1 downto 0);
		Regb, Rega, ea : in  std_logic_vector(7 downto 0);
		RamOut         : out std_logic_vector(7 downto 0)
	);
end entity MemoryS;

-- take care of the usage of when else 
architecture MA2 of MemoryS is
	component ram is
		port(
			clk     : in  std_logic;
			we      : in  std_logic;
			address : in  std_logic_vector(7 downto 0);
			datain  : in  std_logic_vector(7 downto 0);
			dataout : out std_logic_vector(7 downto 0));
	end component;

	signal S_address : std_logic_vector(7 downto 0);
	signal S_w       : std_logic;
begin
	S_w <= '0' WHEN (opcode = "1100" and ra = "01") or opcode = "1101" --LDD or LDI
		ELSE '1' WHEN (opcode = "1100" and ra = "10") or opcode = "1110" --STD or STI
		ELSE '0';                       --LDM and others

	S_address <= ea WHEN opcode = "1100" and (ra = "01" or ra = "10") --ldd or std
		ELSE Rega WHEN opcode = "1101" or opcode = "1110" --LDI or SDI
		ELSE "00000000";                --LDM and other instructions

	u : ram port map(Clk, S_W, S_address, Regb, RamOut);

end architecture MA2;



